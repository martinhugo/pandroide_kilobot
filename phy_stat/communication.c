/**
 * \file communication.c
 * \brief  Contains all the communication function
 * \author { Maud BEGUEY, Hugo MARTIN }
 * \version 1.0
 * \date May 9 2017
 */

 #include <kilombo.h>
#include <stdio.h>

#include "main.h"
#include "communication.h"
#include "params.h"

extern USERDATA * mydata;


/**
 * \brief message rx callback function. Pushes message to ring buffer.
 * \param msg the received message
 * \param dist the measured distance
 * 
 * This method has been taken from Kilombo's examples.
 */
void rxbuffer_push(message_t *msg, distance_measurement_t *dist){
    received_message_t *rmsg = &RB_back();
    rmsg->msg = *msg;
    rmsg->dist = *dist;
    RB_pushback();
}

/**
 * \brief Returns the message to be transmitted
 * \return the message to be transmitted
 * 
 * This method has been taken from Kilombo's examples.
 */
message_t *message_tx() {
    if (mydata->message_lock)
        return 0;
    return &mydata->transmit_msg;
} 

/** 
 * \brief Processes a received message at the front of the ring buffer.
 * 
 * Goes through the list of neighbors. If the message is from a bot already in the list, update the information.
 * 0therwise adds a new entry in the list
 */
void processMessage() {
    uint8_t i;
    uint16_t ID;
    uint8_t d = estimate_distance(&RB_front().dist);

    uint8_t *data = RB_front().msg.data;
    ID = data[0] | (data[1] << 8);

    // search the neighbor list by ID
    uint8_t found = 0;
    i = 0;
    while(i < mydata->N_Neighbors && found==0){
        if (mydata->neighbors[i].ID == ID) {    
            found = 1;
        }
        else{
            i++;
        }
    }

    if (found == 0){          // this neighbor is not in list
        if (mydata->N_Neighbors < MAXN-1)   // if we have too many neighbors, we overwrite the last entry
            mydata->N_Neighbors++;          // sloppy but better than overflow

    }


    // i now points to where this message should be stored
    mydata->neighbors[i].ID = ID;
    mydata->neighbors[i].timestamp = kilo_ticks;
    mydata->neighbors[i].dist = d;
    mydata->neighbors[i].N_Neighbors = data[2];
    mydata->neighbors[i].behavior = data[3];

    //printf("Received: %d %d\n", mydata->neighbors[i].ID, mydata->neighbors[i].behavior);
}

/**
 * \brief Go through the list of neighbors, remove entries older than a threshold, currently 2 seconds.
 */
void purgeNeighbors(void){
    int8_t i;

    for (i = mydata->N_Neighbors-1; i >= 0; i--){
        if (kilo_ticks - mydata->neighbors[i].timestamp  > SECOND){  //this one is too old.
            // We lost the best neighbor
            if(i == mydata->indexBestNeighbor){
                 mydata->bestNeighbor = -1;
                 mydata->indexBestNeighbor = mydata->N_Neighbors-1;
            }
            //the best neighbor is the last and will replace the lost neighbor
            else if(mydata->indexBestNeighbor == mydata->N_Neighbors-1){
                mydata->indexBestNeighbor = i;
            }

            mydata->neighbors[i] = mydata->neighbors[mydata->N_Neighbors-1]; 
            mydata->N_Neighbors--;
          }
    }
}

/**
 *  \brief Return 1 if the robot has a best neighbor.
 *           If a best neighbor is found, mydata->bestNeighbor is updated with the neighbor's ID 
 *           and mydata->indexBestNeighbor with the neighbor's index
 */
uint8_t hasBestNeighbor(){
    int8_t i;
    mydata->indexBestNeighbor = mydata->N_Neighbors-1;
    
    for (i = mydata->N_Neighbors-1; i >= 0; i--) {
        if (mydata->neighbors[i].N_Neighbors >= mydata->neighbors[mydata->indexBestNeighbor].N_Neighbors){
            mydata->indexBestNeighbor = i;
            mydata->bestNeighbor = mydata->neighbors[i].ID;
        }
    }
    return (mydata->bestNeighbor !=-1);
}



/**
 * \brief Sets up the message to be transmitted
 */
void setupMessage(){
    mydata->message_lock = 1;  //don't transmit while we are forming the message
   
    mydata->transmit_msg.type = NORMAL;
    mydata->transmit_msg.data[0] = kilo_uid & 0xff; //0 low  ID
    mydata->transmit_msg.data[1] = kilo_uid >> 8;   //1 high ID
    mydata->transmit_msg.data[2] = mydata->N_Neighbors;     //2 number of neighbors
    mydata->transmit_msg.data[3] = mydata->behavior; //behavior
    mydata->transmit_msg.crc = message_crc(&mydata->transmit_msg);

    mydata->message_lock = 0;
}
/**
 * \brief return the majority state of all neighbors
 * \return 1 if the majority state is aggregation, -1 otherwise.
*/
uint8_t getMajorityBehavior(){
    int16_t nbVote = mydata->behavior*2;
    uint8_t i;
    
    // ++ état * 2
    for(i = 0; i< mydata->N_Neighbors; i++){
        // printf("%d\n", )
        nbVote += mydata->neighbors[i].behavior;
    }

    // ajouter valeur aléatoire si nombre impair
    if(nbVote == 0){
        return (rand_hard()%2);
    }

    uint8_t aggregationMajority = (nbVote > 0);
    return 2*aggregationMajority - 1; // return 1 for aggregation -1 for RW
}