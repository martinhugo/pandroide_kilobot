/**
  * \file main.c
  * \brief Main file of the aggregation-coverage repository
  * \author { Maud BEGUEY, Hugo MARTIN }
  * \version 1.0
  * \date May 9 2017
  */

#include <kilombo.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#include "main.h"
#include "communication.h"
#include "aggregation.h"
#include "random_walk.h"
#include "params.h"

#define MIN(a,b) (a < b) ? a : b
#define MAX(a,b) (a < b) ? b : a

REGISTER_USERDATA(USERDATA) 

// rainbow colors
const uint8_t colors[] = {
    RGB(0,0,0),  //0 - off
    RGB(2,0,0),  //1 - red
    RGB(2,1,0),  //2 - orange
    RGB(2,2,0),  //3 - yellow
    RGB(1,2,0),  //4 - yellowish green
    RGB(0,2,0),  //5 - green
    RGB(0,1,1),  //6 - cyan
    RGB(0,0,1),  //7 - blue
    RGB(1,0,1),  //8 - purple
    RGB(3,3,3)   //9  - bright white
 };




/**
 * \brief Initial setup for the current robot. This method is called before the main loop is started.
 */
void setup(){
    srand(time(NULL) + rand_hard() + kilo_uid);

    mydata->direction = STRAIGHT;
    double random = rand() / (double)RAND_MAX;

    if(random <= 0.5 ){
        mydata->behavior = AGGREGATION;
        mydata->state = SEARCHING;
    }
    else{
        mydata->behavior = RANDOM_WALK;
        mydata->state = 0;
   }
        
    mydata->lastMotionUpdate = 0;
    mydata->N_Neighbors = 0;
    mydata->lastDistUpdate = 0;
    mydata->indexBestNeighbor = 0;
    mydata->bestNeighbor = -1;   
    mydata->directionTicks = 0;
    mydata->avgDist = 0;
    mydata->lastBehaviorMotionUpdate = 0;

    setupMessage();
}



void loop(){
    if(kilo_ticks > mydata->lastMotionUpdate + SECOND){
        mydata->lastMotionUpdate = kilo_ticks;
        while (!RB_empty()) {
            processMessage();
            RB_popfront();
        }
        purgeNeighbors();
        setupMessage();

        //Ajouter un temps entre le nouveau vote et l'ancien ? 
        if(mydata->N_Neighbors > 0 && mydata->lastBehaviorMotionUpdate  + 20*SECOND < kilo_ticks){
            mydata->lastBehaviorMotionUpdate  = kilo_ticks; 

            int8_t majorityBehavior = getMajorityBehavior();
            double random = rand() / (double)RAND_MAX;


            if(random <= PROB_REJECTING_VOTE){

                if(majorityBehavior == RANDOM_WALK){
                    setBehavior(AGGREGATION);
                }
                else{
                    setBehavior(RANDOM_WALK);
                }
            }

            else{
                setBehavior(majorityBehavior);
            }
        }
        


        if(mydata->behavior == RANDOM_WALK){
            randomWalk();
            set_color(colors[8]);
        }
        else{
            aggregation();
            set_color(colors[5]);
        }
        //set_color(colors[mydata->behavior]);
    }
    
}


void setBehavior(int8_t behavior){
    if(mydata->behavior != behavior){
        mydata->behavior = behavior;

        if(behavior == RANDOM_WALK){
            mydata->direction = STRAIGHT;
            mydata->directionTicks = 0;
        }
        else{
            mydata->state = SEARCHING;
        }
    }
}


#ifdef SIMULATOR

#include <jansson.h>

/**
 * \brief Creates a light circle where the intensity varies between 0 at the edge to 1024 at the center. The center is at 0.
 * \param x the x coordinate of the robot
 * \param y the y coordinate of the robot
 * \return the light measure corresponding to the robot position.
 */
int16_t callback_lighting(double x, double y){
    return MAX(1023  - 7/4*(MIN(abs(x),512) + MIN(abs(y), 512)), 0);
}

// int16_t callback_obstacles(double x, double y, double *m1, double *m2){
//     if(x*x + y*y >= 1024*1024){
//         *m1 = (x<0)? 1:-1;
//         *m2 =  (y<0)? 1:-1;
//         return 1;
//     }
//     else{
//         return 0;   
//     }
// }

int16_t callback_obstacles(double x, double y, double *m1, double *m2){
    if(x < - 1000 || x > 1000 || y < -700 || y > 700){
        *m1 = (x<0)? 1:-1;
        *m2 =  (y<0)? 1:-1;
        return 1;
    }
    else{
        return 0;   
    }
}


static char botinfo_buffer[10000];

/**
 * \brief Returns the string to display for the simulator
 * \return the char* corresponding to the string to display
 */
char* botinfo(void){

    char *p = botinfo_buffer;
    p += sprintf (p, "ID: %d - N_Neighbors: %d Behavior %d State: %d\n", kilo_uid,  mydata->N_Neighbors, mydata->behavior, mydata->state);

    return botinfo_buffer;
}
#endif



/**
 * \brief Main function of the behaviors. It calls the function depending of the parameters in the "params.h" file.
 * \return 0 if the behavior has ended normally
 */
int main(void){
    
    kilo_init();
    // initialize ring buffer
    RB_init();
    kilo_message_rx = rxbuffer_push; 
    kilo_message_tx = message_tx;   // register our transmission function
    
    SET_CALLBACK(botinfo, botinfo);
    SET_CALLBACK(reset, setup);
    //SET_CALLBACK(lighting, callback_lighting);
    //SET_CALLBACK(obstacles, callback_obstacles);
    //SET_CALLBACK(json_state, json_state);
    kilo_start(setup, loop);
    
    return 0;
}
