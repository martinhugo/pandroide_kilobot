
#include <kilombo.h>
#include <stdio.h>

#include "main.h"
#include "movement.h"
#include "params.h"

extern USERDATA * mydata;

/** 
  * \brief  Sets a random direction to the robot. Only the right or left direction can be chosen with a 1/2 probability
*/
void turnRandomly(){
    uint8_t random_number = rand_hard()%2;
    if(random_number == 0){
        mydata->direction = LEFT;
    }
    else{
        mydata->direction = RIGHT;
    }
}

/** 
 *  \brief Sets a random direction to the robot. The robots goes straight  with an 1/2 probability and right or left with an 1/4 probability.
 */
void setRandomDirection(){
    uint8_t random_number = rand_hard()%4;
    //Compute the remainder of random_number when divided by 3.
    // This gives a new random number in the set {0, 1, 2}. The value is incremented and returned.
    if ((random_number == 0) || (random_number == 3)){
        mydata->direction = STRAIGHT;
    }
    else if(random_number ==1){
        mydata->direction = LEFT;
    }
    else{
        mydata->direction = RIGHT;
    }
}

/** 
 * \brief A helper function for setting motor state.
 * \param ccw the left motor value
 * \param cw the right motor value
 * 
 * Automatic spin-up of left/right motor, when necessary.
 * The standard way with spinup_motors() causes a small but noticeable jump when a motor which is already running is spun up again.
 * This method has been taken from Kilombo's examples.
 */
void smoothSetMotors(uint8_t ccw, uint8_t cw){
    // OCR2A = ccw;  OCR2B = cw;  
    #ifdef KILOBOT 
        uint8_t l = 0, r = 0;
        if (ccw && !OCR2A) // we want left motor on, and it's off
            l = 0xff;
        if (cw && !OCR2B)  // we want right motor on, and it's off
            r = 0xff;
        if (l || r)        // at least one motor needs spin-up
         {
            set_motors(l, r);
            delay(15);
          }
    #endif

    // spin-up is done, now we set the real value
    set_motors(ccw, cw);
}

/** 
 * \brief Sets the robot in motion according to the mydata->direction value.
 * 
 * This value has been previously set by one of the method during the switch case on mydata->state
 */
void setMotion(){
    switch (mydata->direction){
        case LEFT:
            smoothSetMotors(kilo_turn_left, 0); 
            break;
        case RIGHT:
            smoothSetMotors(0, kilo_turn_right);
            break;
        case STRAIGHT:
            smoothSetMotors(kilo_straight_left, kilo_straight_right);
            break;
        case STOP:
        default:
            smoothSetMotors(0, 0);
            break;
    }
}