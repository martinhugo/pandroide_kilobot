#ifndef MOVEMENT_H
    #define MOVEMENT_H

    void turnRandomly();
    void setRandomDirection();
    void smoothSetMotors(uint8_t ccw, uint8_t cw);
    void setMotion();

#endif
