/**
  * \file random_walk.c
  * \brief This file contains all the method used to adopt a random walk behavior
  * \author { Maud BEGUEY, Hugo MARTIN }
  * \version 1.0
  * \date May 9 2017
  *
  * The random walk behavior is based on Rayleigh Walks.
  */

#include <kilombo.h>

#include "main.h"
#include "movement.h"
#include "random_walk.h"
#include "params.h"


extern USERDATA * mydata;

/**
 * \brief the robot moves on a Rayleigh Walk. 
 * 
 * It turns for a random amount of time, then it moves straight for a fixed amount of time.
 */ 
void randomWalk(){
    //printf("random walk: %d %d\n", mydata->directionTicks, kilo_ticks);
    if(mydata->direction == STRAIGHT && mydata->directionTicks < kilo_ticks){
       //printf("TURN\n");
       setTurnMovement();
    }
    else if(mydata->direction != STRAIGHT && mydata->direction != STOP && mydata->directionTicks < kilo_ticks){
        //printf("STRAIGHT\n");
        setStraightMovement();
    }

    //printf("SET MOTION RANDOM\n");
    setMotion();
}


/**
 * \brief Sets all the robot's settings to make it adopt a straight movement for a fixed amount of time
 *
 */
void setStraightMovement(){
    mydata->direction = STRAIGHT;
    mydata->directionTicks = kilo_ticks + rand_hard();
    setMotion();
}

/**
 * \brief Sets all the settings of the robot to make it adopt a turn movement for a random amount of time
 *
 */
void setTurnMovement(){
    uint8_t nbSecondMax = 15;
    uint16_t turnTime = (rand_hard()%nbSecondMax) * 32;
    //printf("Random time: %d\n", turnTime);
    mydata->directionTicks = kilo_ticks + turnTime;

    //printf("directionTicks: %d Kiloticks: %d\n", mydata->directionTicks, kilo_ticks);
    turnRandomly();
}