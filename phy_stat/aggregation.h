#ifndef AGGREGATION_H
    #define AGGREGATION_H

    uint8_t isTooCloseAggregation();
    void converging();
    void searching();
    void convergingAvgDist();
    uint8_t getAverageDist();
    void turnConverging();
    void aggregation();
    void bestNeighborConverging();
    
#endif
