/**
 * \file aggregation.c
 * \brief Implementation of the Parametrized Probabilistic Aggregation algorithm
 * \author { Maud BEGUEY, Hugo MARTIN }
 * \version 1.0
 * \date May 9 2017
 *
 * Parametrized probabilistic aggregation.
 * This behavior can be used on a Kilobot's swarm,  disposed randomly, in order to aggregate them.
 * This algorithm is based on the algorithm presented in the following article:
 * O. Soysal and E. Sahin, "Probabilistic aggregation strategies in swarm robotic systems," Proceedings 2005 IEEE Swarm Intelligence Symposium, pp. 325-332, 2005.
 */


#include <kilombo.h>
#include <stdio.h>

#include "main.h"
#include "random_walk.h"
#include "aggregation.h"
#include "movement.h"
#include "communication.h"
#include "params.h"

extern USERDATA * mydata;

/** 
 * \brief This function detects if a robot is too close from the current robot. This method is only used in the aggregation behavior.
 * \return 1 if the robot is too close from an other robot
 */
uint8_t isTooCloseAggregation(){
    int8_t i;
    uint8_t stop = 0;
    for(i = mydata->N_Neighbors-1; i>=0; i--){
        if(mydata->neighbors[i].dist <= STOP_DIST_AGGREGATION){
            stop += 1;
        }
    }

    return (stop>= NB_NEIGHBOR_STOP_COND);
}

/**
 * \brief Represents the converging state. 
 *           
 * Changes the direction of the robot depending of the distance to its best neighbor.
 * The robot can decide to swap in the sleeping mode if it has reached its best neighbor.
 */
void converging(){
    // If I'm too close from an other robot
    if(isTooCloseAggregation()==1){
        mydata->direction = STOP;
    }

    // If the best neighbor is still around
    if (mydata->bestNeighbor != -1){
        bestNeighborConverging();
        //convergingAvgDist();
    }
    // if I'm lost, i look for an other best neighbor
    else if(!hasBestNeighbor()){
        mydata->state = SEARCHING;
    }
    else{
        bestNeighborConverging();
        //convergingAvgDist();
    }               
}
 

void convergingAvgDist(){
    uint8_t dist = getAverageDist();

    if(dist > mydata->avgDist){
        
        switch(mydata->direction){
            case RIGHT: 
                mydata->direction = LEFT;
                break;

            case LEFT:
                mydata->direction = RIGHT;
                break;

            case STRAIGHT:
            default:
                turnRandomly();
                break;
        }
    }

    mydata->avgDist = dist;
}

/**
 * \brief The robot doesn't change its direction if the best neighbor is closer, else it changes it to the opposite direction.
 */
void bestNeighborConverging(){
    if (mydata->neighbors[mydata->indexBestNeighbor].dist > mydata->lastDistUpdate){
        switch(mydata->direction){
            case RIGHT: 
                mydata->direction = LEFT;
                break;

            case LEFT:
                mydata->direction = RIGHT;
                break;

            case STRAIGHT:
            default:
                turnRandomly();
                break;
        }
    }

    mydata->lastDistUpdate = mydata->neighbors[mydata->indexBestNeighbor].dist;
}



/**
 * \brief Represents the searching state.
 *   
 * The robot moves in a random correlated walk until a best neighbor has been found.
 */
void searching(){    
    // if I have an interesting neighbor, I try to move toward it
    if (hasBestNeighbor()){
        mydata->state = CONVERGING;
        mydata->lastDistUpdate = mydata->neighbors[mydata->indexBestNeighbor].dist;
    }
    // if I haven't an interesting neighbor, I choose a random direction
    else{
        mydata->state = SEARCHING;
        mydata->bestNeighbor = -1;
        randomWalk();//setRandomDirection();
    } 
}


/** 
 * \brief Main loop of the aggregation behavior.  It contains all behavior it can adopt.
 */
void aggregation() {
    switch(mydata->state){

        // if we are randomly moving
        case SEARCHING: 
            searching();
            break;
       
       // If I'm converging toward an interesting neighbor
        case CONVERGING:
            converging();
            break;

    }

    setMotion();
}

uint8_t getAverageDist(){
    uint16_t avgDist = 0;
    uint8_t i;
    for(i = 0; i<mydata->N_Neighbors;i++){
        avgDist += mydata->neighbors[i].dist;
    }

    return (int) (avgDist/mydata->N_Neighbors);
}


