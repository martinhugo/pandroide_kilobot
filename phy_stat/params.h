/**
  * \file params.h
  * \brief Contains all the parameters applied to the different behaviors.
  * \author { Maud BEGUEY, Hugo MARTIN }
  * \version 1.0
  * \date May 9 2017
  */


#ifndef PARAMETERS_H
    #define PARAMETERS_H
    
    /**
     * \def SECOND
     * \brief number of ticks in a second
     */
    #define SECOND 32  


     /**
     * \def STOP_DIST_AGGREGATION
     * \brief tthe stop distance in the aggregation behavior
     */
    #define STOP_DIST_AGGREGATION  50

        
    /**
     * \def PROB_REJECTING_VOTE
     * \brief the probability of rejecting the vote and following the opposite behavior
     */
    #define PROB_REJECTING_VOTE 0.05

    #define NB_NEIGHBOR_STOP_COND 1
#endif


 