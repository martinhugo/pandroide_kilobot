#ifndef RANDOM_WLK_H
    #define RANDOM_WLK_H
    
    #define STRAIGHT_MOVEMENT_TIME 5*32
    void randomWalk();
    void setStraightMovement();
    void setTurnMovement();

#endif
