/**
 * \file main.h
 * \brief  contains the main prototypes and structures of the aggregation-coverage repository
 * \author { Maud BEGUEY, Hugo MARTIN }
 * \version 1.0
 * \date May 9 2017
 */

#ifndef MAIN__H
    #define MAIN__H
    
    /**
     * \def MAXN 
     * \brief The max number of neighbor (for the list of neighbor)
     */
    #define MAXN 25

    /**
     * \def RB_SIZE
     * \brief the size of the ring buffer
     */
    #define RB_SIZE 128   // Ring buffer size. Choose a power of two for faster code


    /**
     * \enum state
     * \brief the state of  robot in the behavior.
     */
    enum behavior{
        RANDOM_WALK = -1,
        AGGREGATION = 1
    };       

     /**
     * \enum state
     * \brief the state of  robot in the behavior.
     */
    enum state {
        SEARCHING,
        CONVERGING,
        SLEEPING
    };       

     /**
     * \enum direction
     * \brief the robot's direction
     */
    enum direction {
        STOP,                /*!< The robot is stopped*/ 
        LEFT,                 /*!< The robot is moving to the left */ 
        RIGHT,               /*!< The robot is moving to the right */ 
        STRAIGHT         /*!< The robot is moving straight */ 
    };            


    void setup(void);
    void setBehavior(int8_t behavior);
    void loop(void);
    int main(void);
    extern char* (*callback_botinfo) (void);
    char* botinfo(void);
    int16_t callback_lighting(double, double);
    int16_t callback_obstacles(double x, double y, double* , double* );


    /**
     * \struct received_message_t
     * \brief a message labeled by the estimated distance
     *  
     * This structures has been taken from Kilombo's examples.
     */
    typedef struct {
        message_t msg;                                  /*!< the content of the message */ 
        distance_measurement_t dist;         /*!< the measured dist of the message */ 
    } received_message_t;


    /**
     * \struct Neighbor_t
     * \brief neighbor representation. 
     */
    typedef struct {
        uint16_t ID;                     /*!< ID of the neighbor */ 
        uint8_t dist;                    /*!< the last known dist of the neighbor */ 
        
        uint8_t N_Neighbors;
        int8_t behavior;

        uint32_t timestamp;     /*!<the tick where the last information was received from this neighbor ) */ 
    } Neighbor_t;

    /**
     * \struct USERDATA
     * \brief  represents the robot itself and all its beliefs.
    */
    typedef struct 
    {
        Neighbor_t neighbors[MAXN];                         /*!<  List of all neighbors at the current time */
        uint8_t N_Neighbors; 
        uint8_t state;                                                    /*!< The current state of the robot */
        int8_t behavior;                                                 /*!< The current behavior of the robot */
        uint8_t direction;                                                /*!< The current direction of the robot */

        uint8_t indexBestNeighbor;                            /*!< the index of the best neighbor (Parametrized Probabilistic aggregation) */
        int16_t bestNeighbor;                                      /*!< the ID of the best neighbors */ 
        uint8_t lastDistUpdate;                                 /*!< last clock update for converging */
        uint32_t directionTicks;
        uint8_t avgDist;

        message_t transmit_msg;                                /*!< the message to transmit */
        uint32_t lastMotionUpdate;                          /*!< last memorized clock */
        uint32_t lastBehaviorMotionUpdate; 
        char message_lock;                                          /*!< lock on the message to transmit */ 
        received_message_t RXBuffer[RB_SIZE];      /*!< ring buffer of received message */ 
        uint8_t RXHead, RXTail;                                    /*!< start and end of the ring buffer */ 
    } USERDATA;




#endif