#ifndef COMMUNICATION_H
    #define COMMUNICATION_H

    void rxbuffer_push(message_t *msg, distance_measurement_t *dist);
    message_t *message_tx(void);
    void processMessage (void);
    void purgeNeighbors(void);
    uint8_t hasBestNeighbor(void);
    void setupMessage(void);
    uint8_t getMajorityBehavior();

    
    // Ring buffer operations. Taken from kilolib's ringbuffer.h
    // but adapted for use with mydata->

    // Ring buffer operations indexed with head, tail
    // These waste one entry in the buffer, but are interrupt safe:
    //   * head is changed only in popfront
    //   * tail is changed only in pushback
    //   * RB_popfront() is to be called AFTER the data in RB_front() has been used
    //   * head and tail indices are uint8_t, which can be updated atomically
    //     - still, the updates need to be atomic, especially in RB_popfront()

    #define RB_init() { \
        mydata->RXHead = 0; \
        mydata->RXTail = 0;\
    }

    #define RB_empty() (mydata->RXHead == mydata->RXTail)

    #define RB_full()  ((mydata->RXHead+1)%RB_SIZE == mydata->RXTail)

    #define RB_front() mydata->RXBuffer[mydata->RXHead]

    #define RB_back() mydata->RXBuffer[mydata->RXTail]

    #define RB_popfront() mydata->RXHead = (mydata->RXHead+1)%RB_SIZE;

    #define RB_pushback() {\
        mydata->RXTail = (mydata->RXTail+1)%RB_SIZE;\
        if (RB_empty())\
          { mydata->RXHead = (mydata->RXHead+1)%RB_SIZE;  \
        printf("Full.\n"); }        \
    }

#endif
