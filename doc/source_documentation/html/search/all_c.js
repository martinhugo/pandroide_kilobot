var searchData=
[
  ['rb_5fsize',['RB_SIZE',['../main_8h.html#a4ab5f7800f96dec4076a4a0c8aa634b7',1,'main.h']]],
  ['received_5fmessage_5ft',['received_message_t',['../structreceived__message__t.html',1,'']]],
  ['repelling',['repelling',['../aggregation_8c.html#a8a036a6f5df5d367ec88b45c8b79a31c',1,'repelling():&#160;aggregation.c'],['../main_8h.html#adc6e5733fc3c22f0a7b2914188c49c90afc498acda60d19ecf03a2dc1406b086a',1,'REPELLING():&#160;main.h']]],
  ['repelling_5fmethod',['REPELLING_METHOD',['../params_8h.html#a66852fbefb96cc959e289fb3096866fa',1,'params.h']]],
  ['right',['RIGHT',['../main_8h.html#a99f26e6ee9fcd62f75203b5402df8098aec8379af7490bb9eaaf579cf17876f38',1,'main.h']]],
  ['robust_5fcluster_5fsize',['ROBUST_CLUSTER_SIZE',['../params_8h.html#a5c684841005b6c375dfb58ac03cc60fc',1,'params.h']]],
  ['rxbuffer',['RXBuffer',['../struct_u_s_e_r_d_a_t_a.html#a8d26def60d1ca206ba9364104854c300',1,'USERDATA']]],
  ['rxbuffer_5fpush',['rxbuffer_push',['../communication_8c.html#a5760adaea4991d218d6cea8486847748',1,'communication.c']]],
  ['rxtail',['RXTail',['../struct_u_s_e_r_d_a_t_a.html#a83f09a01f3260e6623a63b09e377d8e3',1,'USERDATA']]]
];
