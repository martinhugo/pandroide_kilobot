#ifndef AGGREGATION_H
    #define AGGREGATION_H
    void sleeping();
    void converging();
    void repelling();
    void searching();
    void aggregation(void);
    uint8_t isTooCloseAggregation(void);
#endif
