/**
 * \file probability.c
 * \brief Contains all the probability measures and proability-based decision implemented for the behaviors
 * \author { Maud BEGUEY, Hugo MARTIN }
 * \version 1.0
 * \date May 9 2017
 */

#include <kilombo.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "main.h"
#include "aggregation.h"
#include "probability.h"
#include "params.h"

extern USERDATA *mydata;

/**
 * \brief This function calculates the probability according to a sigmoid function.
 * \param nbNeighbors the number of neighbor for which this probability is calculated
 * \param error the value of the probability at 0
 * \return The probability which coresponds to nbNeighbors.
*/
double getSigmoidProbJoining(int nbNeighbors, double error){
    double b = log((1/error) - 1);
    double a = (-log(error) + b) / ROBUST_CLUSTER_SIZE;
    return 1/(1+exp(-a * nbNeighbors + b));
}

/**
 * \brief This function calculates the probability according to a linear function.
 * \param nbNeighbors the value for which the probability is calculated
 * \return The probability which coresponds to nbNeighbors.
 */
double getLinearProbJoining(int nbNeighbors){
    return nbNeighbors/(double)ROBUST_CLUSTER_SIZE;
}

/**
 * \brief Returns the probability according to a logarithmic function. 
 * \param nbNeighbors the value for which the probability is calculated
 * \return The probability which coresponds to nbNeighbors.
 */
double getLogProbJoining(int nbNeighbors){
    return  log10(nbNeighbors + 1)/log10(nbNeighbors + 10); 
}


/** 
 * \return probability of joining the neighbor in the current position
 * \brief Return the probability  calculated by the method depending of the JOINING_PROB_METHOD  parameter in the "params.h" file.
 */
double getProbJoining(){
    uint8_t nbNeighborsBestNeighbor= mydata->neighbors[mydata->indexBestNeighbor].N_Neighbors;
    double p;

    switch(JOINING_PROB_METHOD){
        case LOG: 
            p = getLogProbJoining(nbNeighborsBestNeighbor);
            break;
        case SIGMOID:
            p = getSigmoidProbJoining(nbNeighborsBestNeighbor, 0.01);
            break;
        case CONSTANT:
            p = CONSTANT_REPEL_PROBA;
            break;
        default:
            p = getLinearProbJoining(nbNeighborsBestNeighbor);
            break;
    }

    return (p<1)? p:1;
}

/** 
 * \return probability of leaving the staying in the current position
 * \brief Calls the probability method depending of the LEAVING_PROB_METHOD parameter in the "params.h" file.
 */
double getProbLeaving(){
    double p;
    double nbNeighbors = mydata->N_Neighbors;
    switch(LEAVING_PROB_METHOD){
        case LOG: 
            p = getLogProbJoining(nbNeighbors);
            break;
        case SIGMOID:
            p = getSigmoidProbJoining(nbNeighbors, 0.01);
            break;
        case CONSTANT:
            p = 1 - CONSTANT_LEAVING_PROBA;
            break;
        default:
            p = getLinearProbJoining(nbNeighbors);
            break;
    }
    return 1-p;
}

/** 
 *  \brief Sets the state and direction of the robot following the probJoining probability
 * 
 *  A random number is calculated and if it's under the probability, the robot stops.
 *  If it's not, the robot is moving randomly and go in the searching state.
 */
void makeJoinDecision(){
    double p = getProbJoining();
    double random = rand()/(double)RAND_MAX;

    if (random < p){
        mydata->state = CONVERGING;
        mydata->direction =  rand_hard()%2 + 1;
    }

}

/** 
 *  \brief Sets the state and direction of the robot following the probLeaving probability
 *           
 * A random number is calculated and if it's under the probability, the robot is leaving the cluster
 * If it's not, the robot stays in the sleeping state.
 */
void makeLeaveDecision(){
    if (rand()/(double)RAND_MAX < getProbLeaving()){
        mydata->state = REPELLING;
        mydata->direction = rand_hard()%2 + 1;
        mydata->start_repelling = kilo_ticks;
    }
}
