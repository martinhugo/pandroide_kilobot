/**
  * \file beeclust.c
  * \brief Implementation of the Beeclust algorithm
  * \author { Maud BEGUEY, Hugo MARTIN }
  * \version 1.0
  * \date May 9 2017
  *
  * Beeclust aggregation
  * This behavior can be used on a Kilobot's swarm, disposed randomly, in order to aggregate them depending on the light level. 
  * This algorithm is based an implementation of the algorithm presented in the following article:
  * F. Arvin , K. Samsudin , A. R. Ramli and M. Bekravi  "Imitation of Honeybee Aggregation with Collective Behavior of Swarm Robots". International Journal of Computational Intelligence Systems, vol. 4, no. 4 pp. 739–748, 2011.
  *               
  */

#include <math.h>
#include <kilombo.h>
#include <stdio.h>

#include "main.h"
#include "beeclust.h"
#include "communication.h"
#include "movement.h"
#include "params.h"

extern USERDATA * mydata;


/** 
 * \brief Sets the mydata->current_light value as the mean of 300 light's samples. 
 */
void sample_light(){
    // The ambient light sensor gives noisy readings. To mitigate this,
    // we take the average of 300 samples in quick succession.
    int16_t number_of_samples = 0;
    int32_t sum = 0;

    while (number_of_samples < 300)
    {
        int16_t sample = get_ambientlight();
        // -1 indicates a failed sample, which should be discarded.
        if (sample != -1)
        {
            sum = sum + sample;
            number_of_samples = number_of_samples + 1;
        }
    }

    // Compute the average.
    mydata->current_light = sum / number_of_samples;
}



/**
* \brief This function sets the duration sleep of the robots depending on its lighting level
*/
void setsDurationSleep(){
    // uint32_t pow_light = pow(mydata->current_light, 2);
    // mydata->waiting_time= MAX_WAITING_TIME * pow_light/(pow_light + STEEPNESS);
    mydata->waiting_time= MAX_WAITING_TIME * mydata->current_light/1024;
}



/**
 * \brief This function sets the robots in the sleeping state. 
 */
void setsSleepState(){
    // the robot considered the best neighbor (normally with get_ambientlight())
    setsDurationSleep();
    mydata->state = SLEEPING;
    mydata->direction = STOP;
}
    


/** 
 * \brief This function detects if the current robot is too close from a neighbor. This method is used only in beeclust.
 * \return 1 if the robot if too close from an other robot, else 0.
 */
uint8_t isTooCloseBeeclust(){
    int8_t i;
    uint8_t stop = 0;
    for(i = mydata->N_Neighbors-1; i>=0; i--){ // for each neighbor

        if(mydata->neighbors[i].dist <= STOP_DIST_BEECLUST){ // if a robot is too close

            if((mydata->neighbors[i].ID != mydata->lastNeighbor) || (mydata->last_sleeping_state + TIME_TO_IGNORE_LAST_NEIGHBOR * SECOND <= kilo_ticks)){ // if it isn't its last neighbor or the time to ignore its last neighbor isn't valide
                stop = 1;
                // history of the neighbor considered for the sleeping state
                mydata->lastNeighbor = mydata->neighbors[i].ID; 

            }
        }
    }

    return stop;
}




/** 
 * \brief Main loop of the current robot. Contains all behavior it can adopt.
 */
void beeclust() {
    // Message management and neighbors updating, needs to be done no matter what the state is
    while (!RB_empty()) {
        process_message();
        RB_popfront();
    }
    purgeNeighbors();
    sample_light();
    // Picking a direction depending of its internal state

    switch(mydata->state){
        // the kilobot moved at random while it meet a neighbor
        case SEARCHING: 
            set_color(RGB(0, 0, 0));
            if(kilo_ticks > mydata->last_motion_update + SECOND){
                mydata->last_motion_update = kilo_ticks;

                // if the robot as a neighbor
                if(isTooCloseBeeclust()){
                    setsSleepState();
                }

                // if the robot hasn't meet an other robot
                else{
                    setRandomDirection();
                }
            }
            break;
            
        case SLEEPING: 
            set_color(RGB(0, 0, 1));
            // if the time into sleeping state is exceed
            if(kilo_ticks > mydata->last_motion_update + mydata->waiting_time*SECOND){
               mydata->last_motion_update = kilo_ticks;
               mydata->last_sleeping_state = kilo_ticks;

               mydata->state = SEARCHING;
               setRandomDirection();
            }  
            break; 
    }
    set_motion();
}


