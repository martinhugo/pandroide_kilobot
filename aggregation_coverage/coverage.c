/**
 * \file coverage.c
 * \brief Implementation of the Naive Coverage
 * \author { Maud BEGUEY, Hugo MARTIN }
 * \version 1.0
 * \date May 9 2017
 *
 * Naive coverage.
 * This behavior can be used on a Kilobot's swarm, in a random or aggregated disposition.
 */

#include <math.h>
#include <kilombo.h>
#include <stdio.h>

#include "coverage.h"
#include "communication.h"
#include "movement.h"
#include "main.h"
#include "params.h"

#ifndef SIMULATOR
    #include <avr/io.h>      // for microcontroller register defs 
#endif

extern USERDATA * mydata;

    
/** 
 * \brief This function detects if the robot if too close from an other robot.
 * \return 1 if the robot is too close from an other robot, else 0.  
 */
uint8_t isTooCloseCoverage(){
    int8_t i;
    uint8_t stop = 0;
    for(i = mydata->N_Neighbors-1; i>=0; i--){
        if(mydata->neighbors[i].dist <= STOP_DIST_COVERAGE){
            stop = 1;
        }
    }

    return stop;
}

/** 
 * \brief Main loop of the current robot. Contains all behavior it can adopt.
 */
void coverage() {
    // Message management and neighbors updating, needs to be done no matter what the state is
    while (!RB_empty()) {
        process_message();
        RB_popfront();
    }
    purgeNeighbors();
    setupMessage();

    //verification every seconds
    if(kilo_ticks > mydata->last_motion_update + SECOND){
        mydata->last_motion_update = kilo_ticks;
        
        // if the robot is too close from its neighbors
        if(isTooCloseCoverage() || mydata->N_Neighbors == 0){
            setRandomDirection();
            mydata->state = SEARCHING;
            set_color(RGB(0, 1, 0));
        }

        //if the robot hasn't any neighbor too close.
        else{
            mydata->state = SLEEPING;
            mydata->direction = STOP;
            set_color(RGB(0,0,1));
        }
    }
                    
    set_motion();
}