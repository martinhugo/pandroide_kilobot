#ifndef BEECLUST_H
    #define BEECLUST_H

    void setsDurationSleep(void);
    void sample_light(void);
    void setRandomDirection(void);
    void setsSleepState(void);
    uint8_t isTooCloseBeeclust(void);
    void beeclust(void);

#endif
