/**
 * \file movement.c
 * \brief Contains all the movement behaviors implemented for the behaviors
 * \author { Maud BEGUEY, Hugo MARTIN }
 * \version 1.0
 * \date May 9 2017
 */
#include <kilombo.h>
#include <stdio.h>

#include "main.h"
#include "movement.h"
#include "params.h"

#ifndef SIMULATOR
    #include <avr/io.h>      // for microcontroller register defs 
#endif

extern USERDATA *mydata;

/** 
 *  \brief Sets a random direction to the robot. The robots goes straight  with an 1/2 probability and right or left with an 1/4 probability.
 */
void setRandomDirection(){
    uint8_t random_number = rand_hard()%4;
    //Compute the remainder of random_number when divided by 3.
    // This gives a new random number in the set {0, 1, 2}. The value is incremented and returned.
    if ((random_number == 0) || (random_number == 3)){
        mydata->direction = STRAIGHT;
    }
    else if(random_number ==1){
        mydata->direction = LEFT;
    }
    else{
        mydata->direction = RIGHT;
    }
}

/** 
  * \brief  Sets a random direction to the robot. Only the right or left direction can be chosen with a 1/2 probability
  */
void turnRandomly(){
    uint8_t random_number = rand_hard()%2;
    if(random_number == 0){
        mydata->direction = LEFT;
    }
    else{
        mydata->direction = RIGHT;
    }
}


/** 
 * \brief This function detects if a robot is in collision with the current robot. This method is used in all behaviors except Parametrized Probabilistic Aggregation
 * \return 1 if the robot is in collision with an other robot, 0 otherwise 
 */
uint8_t isInACollision(){
    int8_t i;
    uint8_t stop = 0;
    for(i = mydata->N_Neighbors-1; i>=0; i--){
        if(mydata->neighbors[i].dist <= COLLISION_DIST){
            stop = 1;
        }
    }

    return stop;
}

/**
 * \brief Changes the direction of the robot to the right  if the robot is closer from its best neighbor, to the right otherwise.
 */
void constantConverging(){
    if (mydata->neighbors[mydata->indexBestNeighbor].dist > mydata->last_dist_update){
        mydata->direction = LEFT;
    }
    else{
        mydata->direction = RIGHT;
    }

    mydata->last_dist_update = mydata->neighbors[mydata->indexBestNeighbor].dist;
}

/**
 * \brief The robot doesn't change its direction if the best neighbor is closer, else it changes it to the opposite direction.
 */
void turnConverging(){
    if (mydata->neighbors[mydata->indexBestNeighbor].dist > mydata->last_dist_update){
        
        switch(mydata->direction){
            case RIGHT: 
                mydata->direction = LEFT;
                break;

            case LEFT:
                mydata->direction = RIGHT;
                break;

            case STRAIGHT:
            default:
                turnRandomly();
                break;
            }
        }

    mydata->last_dist_update = mydata->neighbors[mydata->indexBestNeighbor].dist;
}

/**
 * \brief Moves straight if the best neighbor is closer, turns to the opposite direction otherwise.
 */
void straightConverging(){
    if (mydata->neighbors[mydata->indexBestNeighbor].dist > mydata->last_dist_update){

        switch(mydata->direction){
            case RIGHT: 
                mydata->direction = LEFT;
                break;

            case LEFT:
                mydata->direction = RIGHT;
                break;

            case STRAIGHT:
            default:
                turnRandomly();
                break;
            }
        }

        else{
            mydata->direction = STRAIGHT;
        }

    mydata->last_dist_update = mydata->neighbors[mydata->indexBestNeighbor].dist;
}

/**
 * \brief Changes the direction of the robot to the left if the robot is closer to its best neighbor, to the right otherwise.
 */
void constantRepelling(){
    if (mydata->neighbors[mydata->indexBestNeighbor].dist <= mydata->last_dist_update){
        mydata->direction = LEFT;
    }
    else{
        mydata->direction = RIGHT;
    }

    mydata->last_dist_update = mydata->neighbors[mydata->indexBestNeighbor].dist;
}

/**
 * \brief Changes its direction to the opposite direction if the best neighbor is closer,  else it doesn't change its direction.
 */
void turnRepelling(){
    if (mydata->neighbors[mydata->indexBestNeighbor].dist <= mydata->last_dist_update){

        switch(mydata->direction){
            case RIGHT: 
                mydata->direction = LEFT;
                break;

            case LEFT:
                mydata->direction = RIGHT;
                break;

            case STRAIGHT:
            default:
                turnRandomly();
                break;
            }
        }

    mydata->last_dist_update = mydata->neighbors[mydata->indexBestNeighbor].dist;
}


/**
 * \brief Moves to the opposite direction if the best neighbor is closer, else it moves straight
 */
void straightRepelling(){
    if (mydata->neighbors[mydata->indexBestNeighbor].dist <= mydata->last_dist_update){

        switch(mydata->direction){
            case RIGHT: 
                mydata->direction = LEFT;
                break;

            case LEFT:
                mydata->direction = RIGHT;
                break;

            case STRAIGHT:
            default:
                turnRandomly();
                break;
            }
        }

        else{
            mydata->direction = STRAIGHT;
        }

            mydata->last_dist_update = mydata->neighbors[mydata->indexBestNeighbor].dist;
}


/** 
 * \brief Calls the converging method according to the CONVERGING_METHOD parameter in the "params.h" file
 */
void setConvergingDirection(){
    switch(CONVERGING_METHOD){
    case TURN_METHOD:
        turnConverging();
        break;
    case STRAIGHT_METHOD:
        straightConverging();
        break;
    case CONSTANT_METHOD:
        constantConverging();
        break;
    }
}

/** 
 * \brief Calls the repelling method according to the REPELLING_METHOD parameter in the "params.h" file
 */
void setRepellingDirection(){
    switch(REPELLING_METHOD){
    case TURN_METHOD:
        turnRepelling();
        break;
    case STRAIGHT_METHOD:
        straightRepelling();
        break;
    case CONSTANT_METHOD:
        constantRepelling();
        break;
    }
}



/** 
 * \brief A helper function for setting motor state.
 * \param ccw the left motor value
 * \param cw the right motor value
 * 
 * Automatic spin-up of left/right motor, when necessary.
 * The standard way with spinup_motors() causes a small but noticeable jump when a motor which is already running is spun up again.
 * This method has been taken from Kilombo's examples.
 */
void smooth_set_motors(uint8_t ccw, uint8_t cw){
    // OCR2A = ccw;  OCR2B = cw;  
    #ifdef KILOBOT 
        uint8_t l = 0, r = 0;
        if (ccw && !OCR2A) // we want left motor on, and it's off
            l = 0xff;
        if (cw && !OCR2B)  // we want right motor on, and it's off
            r = 0xff;
        if (l || r)        // at least one motor needs spin-up
         {
            set_motors(l, r);
            delay(15);
          }
    #endif

    // spin-up is done, now we set the real value
    set_motors(ccw, cw);
}

/** 
 * \brief Sets the robot in motion according to the mydata->direction value.
 * 
 * This value has been previously set by one of the method during the switch case on mydata->state
 */
void set_motion(){
    switch (mydata->direction){
        case LEFT:
            smooth_set_motors(kilo_turn_left, 0); 
            break;
        case RIGHT:
            smooth_set_motors(0, kilo_turn_right);
            break;
        case STRAIGHT:
            smooth_set_motors(kilo_straight_left, kilo_straight_right);
            break;
        case STOP:
        default:
            smooth_set_motors(0, 0);
            break;
    }
}
