/**
  * \file params.h
  * \brief Contains all the parameters applied to the different behaviors.
  * \author { Maud BEGUEY, Hugo MARTIN }
  * \version 1.0
  * \date May 9 2017
  */


#ifndef PARAMETERS_H
    #define PARAMETERS_H
    
    /**
     * \def SECOND
     * \brief number of ticks in a second
     */
    #define SECOND 32


    /** 
     * \def BEHAVIOR
     * \brief applied behavior
     */
    #define BEHAVIOR AGGREGATION

   /** 
     * \def BEHAVIOR_TIME
     * \brief Defines   time of each behavior in the loop behavior (seconds)
     */
    #define BEHAVIOR_TIME 3000

    /** 
     * \def ROBUST_CLUSTER_SIZE 
     * \brief  how many neighbors a robot should have to believe it belongs to a strong cluster
     */
    #define ROBUST_CLUSTER_SIZE 5

    /** 
     * \def  CONSTANT_REPEL_PROBA
     * \brief probability of joining back in constant probability measure
     */
    #define CONSTANT_REPEL_PROBA 0.001

    /** 
     * \def  CONSTANT_LEAVING_PROBA
     * \brief probability of leaving in constant probability measure
     */
    #define CONSTANT_LEAVING_PROBA 0.0001


    /**
     * \def JOINING_PROB_METHOD
     * \brief used probability measure to decides to join a neighbor in the leaving state
     */
    #define JOINING_PROB_METHOD SIGMOID

    /**
     * \def LEAVING_PROB_METHOD
     * \brief used probability measure to leave the current cluster
     */
    #define LEAVING_PROB_METHOD LINEAR
         

    /**
     * \def CONVERGING_METHOD
     * \brief used way to approach a cluster
     */
    #define CONVERGING_METHOD TURN_METHOD

    /**
     * \def REPELLING_METHOD
     * \brief used way to leave a cluster
     */
    #define REPELLING_METHOD TURN_METHOD

    /**
     * \def COLLISION_DIST
     * \brief minimal allowed distance in a obstacle avoidance implementation
     */
    #define COLLISION_DIST 30

    /** COVERAGE PARAMETERS **/

    /**
     * \def STOP_DIST_COVERAGE
     * \brief minimum allowed distance between two robots in the naive coverage
     */
    #define STOP_DIST_COVERAGE  45


     /**
     * \def STOP_DIST_BEECLUST
     * \brief stop distance between two robots in beeclust
     */
    #define STOP_DIST_BEECLUST 40

    /**
     * \def TIME_TO_IGNORE_LAST_NEIGHBOR
     * \briefthe number of seconds to ignoremake the last neighbor in beeclust
     */
    #define TIME_TO_IGNORE_LAST_NEIGHBOR 1 // in seconds

    /**
     * \def MAX_WAITING_TIME 
     * \brief the max allowed waiting time in beeclust
     */
    #define MAX_WAITING_TIME 60                     // in seconds


    /**
     * \def STEEPNESS
     * \brief the steepness parameter in the waiting time calculation (beeclust)
     */
    #define STEEPNESS 7000                             

    
     /**
     * \def STOP_DIST_AGGREGATION
     * \brief tthe stop distance in the aggregation behavior
     */
    #define STOP_DIST_AGGREGATION  50


    /**
     * \enum behavior
     * \brief the behavior adopted by the robot at the current time
     */
    enum behavior{
        AGGREGATION,                          /*!< Parametrized Probabilistic Aggregation */ 
        COVERAGE,                                /*!< Naive Coverage*/ 
        BEECLUST,                                /*!< Beeclust */ 
        COVERAGE_AGGREGATION    /*!< Loop Behavior */ 
    };


    /**
     * \enum movementMethod
     * \brief the used movement method
     */
    enum movementMethod{
        TURN_METHOD,                  /*!< Turn method   */ 
        STRAIGHT_METHOD,          /*!< Straight method */ 
        CONSTANT_METHOD         /*!< Constant method  */ 
    };


    /**
     * \enum probMethod
     * \brief the used probability measure
     */
    enum probMethod{
        LOG,                     /*!< Log probability  */ 
        SIGMOID,            /*!< Sigmoid probability */ 
        LINEAR,               /*!< Linear probability  */ 
        CONSTANT         /*!< Constant probability */ 
    };

#endif


