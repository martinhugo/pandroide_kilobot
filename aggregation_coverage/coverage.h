#ifndef COVERAGE_H
    #define COVERAGE_H

    void smooth_set_motors(uint8_t ccw, uint8_t cw) ;
    void set_motion(void);
    uint8_t isTooCloseCoverage(void);
    void coverage(void);
  
#endif
