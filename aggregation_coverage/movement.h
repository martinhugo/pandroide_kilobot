#ifndef MOVEMENT_H
    #define MOVEMENT_H
    void setRandomDirection(void);
    void turnRandomly(void);
    uint8_t isInACollision(void);

    void constantConverging(void);
    void turnConverging(void);
    void straightConverging(void);
    void constantRepelling(void);
    void turnRepelling(void);
    void straightRepelling(void);

    void setConvergingDirection(void);
    void setRepellingDirection(void);
    void smooth_set_motors(uint8_t ccw, uint8_t cw);
    void set_motion(void);

                 
#endif
