/**
 * \file aggregation.c
 * \brief Implementation of the Parametrized Probabilistic Aggregation algorithm
 * \author { Maud BEGUEY, Hugo MARTIN }
 * \version 1.0
 * \date May 9 2017
 *
 * Parametrized probabilistic aggregation.
 * This behavior can be used on a Kilobot's swarm,  disposed randomly, in order to aggregate them.
 * This algorithm is based on the algorithm presented in the following article:
 * O. Soysal and E. Sahin, "Probabilistic aggregation strategies in swarm robotic systems," Proceedings 2005 IEEE Swarm Intelligence Symposium, pp. 325-332, 2005.
 */


#include <kilombo.h>
#include <stdio.h>

#include "main.h"
#include "aggregation.h"
#include "communication.h"
#include "movement.h"
#include "probability.h"
#include "params.h"


extern USERDATA * mydata;

/** 
 * \brief This function detects if a robot is too close from the current robot. This method is only used in the aggregation behavior.
 * \return 1 if the robot is too close from an other robot
 */
uint8_t isTooCloseAggregation(){
    int8_t i;
    uint8_t stop = 0;
    for(i = mydata->N_Neighbors-1; i>=0; i--){
        if(mydata->neighbors[i].dist <= STOP_DIST_AGGREGATION){
            stop = 1;
        }
    }

    return stop;
}


/**
 * \brief Represents the sleeping behavior.
 *  
 * Checks if the robot needs to move away from the cluster or to stay in the sleeping state.
 */
void sleeping(){
    set_color(RGB(0,0,1));
    makeLeaveDecision();
}

/**
 * \brief Represents the converging state. 
 *           
 * Changes the direction of the robot depending of the distance to its best neighbor.
 * The robot can decide to swap in the sleeping mode if it has reached its best neighbor.
 */
void converging(){
    set_color(RGB(0,1,0));

    // If I'm too close from an other robot
    if(isTooCloseAggregation()==1){
        mydata->state = SLEEPING;
        mydata->direction = STOP;
    }
    // If the best neighbor is still around
    else if (mydata->bestNeighbor != -1){
        setConvergingDirection();
    }
    // if I'm lost, i look for an other best neighbor
    else if(!hasBestNeighbor()){
        mydata->state = SEARCHING;
    }
    else{
        setConvergingDirection();
    }               
}

/**
 * \brief Represents the repelling state.
 *  
 * The robot moves away for a fixed amount of time,  and decide at each instant if it converges again to the best neighbor.
 * If the robot is lost, it goes in the searching state.
 */
void repelling(){
    set_color(RGB(1,0,0));
    
    // if the delay of 10 seconds is finished
    if(kilo_ticks > mydata->start_repelling + 10*SECOND){
        mydata->state = SEARCHING;
    }

    // If the best neighbor is still around
    makeJoinDecision();
    if ((mydata->bestNeighbor = -1) && (mydata->state != CONVERGING)){
        setRepellingDirection();
    }

    // if I'm lost
    else if(mydata->state != CONVERGING){
        if(hasBestNeighbor()){
            setRepellingDirection();
        }
        else{
            mydata->state = SEARCHING;
        }
    }               
}

/**
 * \brief Represents the searching state.
 *   
 * The robot moves in a random correlated walk until a best neighbor has been found.
 */
void searching(){
    set_color(RGB(1,1,1));
    mydata->last_motion_update = kilo_ticks;
    
    // if I have an interesting neighbor, I try to move toward it
    if (hasBestNeighbor()){
        mydata->state = CONVERGING;
        mydata->direction = STRAIGHT; 
        mydata->last_dist_update = mydata->neighbors[mydata->indexBestNeighbor].dist;
    }
    // if I haven't an interesting neighbor, I choose a random direction
    else{
        mydata->state = SEARCHING;
        mydata->bestNeighbor = -1;
        setRandomDirection();
    } 
}

/** 
 * \brief Main loop of the aggregation behavior.  It contains all behavior it can adopt.
 */
void aggregation() {
    // Message management and neighbors updating, needs to be done no matter what the state is
    while (!RB_empty()) {
        process_message();
        RB_popfront();
    }
    purgeNeighborsAggregation();
    setupMessage();

    if(kilo_ticks > mydata->last_motion_update + SECOND){
        mydata->last_motion_update = kilo_ticks;
        
        // if(isInACollision()){
        //     set_color(RGB(1,1,0));
        //     setRandomDirection();
        // }
        // else{
            // Picking a direction depending of its internal state 
        switch(mydata->state){

            // if we are randomly moving
            case SEARCHING: 
                searching();
                break;
           
           // If I'm converging toward an interesting neighbor
            case CONVERGING:
                converging();
                break;
                
            case SLEEPING:  
                sleeping();
                break;

            case REPELLING:
                repelling();
                break;
        }
        //}
    }

    set_motion();
}


