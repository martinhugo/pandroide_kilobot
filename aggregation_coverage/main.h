/**
 * \file main.h
 * \brief  contains the main prototypes and structures of the aggregation-coverage repository
 * \author { Maud BEGUEY, Hugo MARTIN }
 * \version 1.0
 * \date May 9 2017
 */

#ifndef MAIN__H
    #define MAIN__H
    
    /**
     * \def MAXN 
     * \brief The max number of neighbor (for the list of neighbor)
     */
    #define MAXN 50

    /**
     * \def RB_SIZE
     * \brief the size of the ring buffer
     */
    #define RB_SIZE 64   // Ring buffer size. Choose a power of two for faster code


     /**
     * \enum state
     * \brief the state of  robot in the behavior.
     */
    enum state {
        SEARCHING,             /*!< The robot is searching for an other robot */ 
        CONVERGING,          /*!< The robot is moving to a neighbor */ 
        SLEEPING,                /*!< The robot is sleeping */ 
        REPELLING               /*!< The robot is moving away from a neighbor */ 
    };       

     /**
     * \enum direction
     * \brief the robot's direction
     */
    enum direction {
        STOP,                /*!< The robot is stopped*/ 
        LEFT,                 /*!< The robot is moving to the left */ 
        RIGHT,               /*!< The robot is moving to the right */ 
        STRAIGHT         /*!< The robot is moving straight */ 
    };            


    void setup(void);
    void loop(void);
    void loopAggregationCoverage(void);
    int main(void);
    extern char* (*callback_botinfo) (void);
    char* botinfo(void);
    int16_t callback_lighting(double, double);
    int16_t callback_obstacles(double x, double y, double* , double* );


    /**
     * \struct received_message_t
     * \brief a message labeled by the estimated distance
     *  
     * This structures has been taken from Kilombo's examples.
     */
    typedef struct {
        message_t msg;                                  /*!< the content of the message */ 
        distance_measurement_t dist;         /*!< the measured dist of the message */ 
    } received_message_t;

    /**
     * \struct Neighbor_t
     * \brief neighbor representation. 
     */
    typedef struct {
        uint16_t ID;                     /*!< ID of the neighbor */ 
        uint8_t dist;                    /*!< the last known dist of the neighbor */ 
        uint8_t N_Neighbors;    /*!< the number of neighbor */ 
        uint32_t timestamp;     /*!<the tick where the last information was received from this neighbor ) */ 
    } Neighbor_t;


    /**
     * \struct USERDATA
     * \brief  represents the robot itself and all its beliefs.
    */
    typedef struct 
    {
        Neighbor_t neighbors[MAXN];                         /*!<  List of all neighbors at the current time */

        uint8_t N_Neighbors;                                         /*!< The number of neighbors */

        uint8_t state;                                                       /*!< The current state of the robot */
        uint8_t direction;                                                /*!< The current direction of the robot */
        uint16_t current_light;                                       /*!< The current light level */

        message_t transmit_msg;                                /*!< the message to transmit */
        uint32_t last_motion_update;                          /*!< last memorized clock */
        uint16_t waiting_time;                                      /*!< Waiting time (for beeclust) */
        uint32_t last_sleeping_state;                          /*!< last memorized clock for repelling */
        uint32_t start_repelling;                                   /*!< Start tick of the repelling behavior */
        
        uint8_t last_dist_update;                                 /*!< last clock update for converging */

        uint16_t lastNeighbor;                                     /*!< The last neighbor the robot was in collision with (beeclust) */
        uint8_t indexBestNeighbor;                            /*!< the index of the best neighbor (Parametrized Probabilistic aggregation) */
        int16_t bestNeighbor;                                      /*!< the ID of the best neighbors */ 

        uint32_t start_time_behavior;                         /*!< the start tick of the current behavior (loop behavior) */ 
        uint8_t behavior;                                               /*!< the current behavior */ 

        char message_lock;                                          /*!< lock on the message to transmit */ 
        received_message_t RXBuffer[RB_SIZE];      /*!< ring buffer of received message */ 
        uint8_t RXHead, RXTail;                                    /*!< start and end of the ring buffer */ 
    } USERDATA;




#endif