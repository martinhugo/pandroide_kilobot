/**
  * \file main.c
  * \brief Main file of the aggregation-coverage repository
  * \author { Maud BEGUEY, Hugo MARTIN }
  * \version 1.0
  * \date May 9 2017
  */

#include <kilombo.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "main.h"
#include "aggregation.h"
#include "beeclust.h"
#include "coverage.h"
#include "communication.h"
#include "params.h"


#define MIN(a,b) (a < b) ? a : b
#define MAX(a,b) (a < b) ? b : a

REGISTER_USERDATA(USERDATA) 

/**
 * \brief Main function of the loop of the alternation between the aggregation and the coverage behavior
 */
void loopAggregationCoverage(){
    if(mydata->start_time_behavior  + ((uint32_t)BEHAVIOR_TIME * (uint32_t)SECOND)<= kilo_ticks){
        mydata->start_time_behavior= kilo_ticks;
        if(mydata->behavior == AGGREGATION){
            mydata->behavior = COVERAGE;
        }
        else{
            mydata->behavior = AGGREGATION;
        }

    }

    if(mydata->behavior == AGGREGATION){
        aggregation();
    }
    else{
        coverage();
    }
}


/** 
 * \brief Initial setup for the current robot. This method is called before the main loop is started.
 */
void setup(){
    setupMessage();
    mydata->N_Neighbors = 0;
    mydata->state = SEARCHING;

    mydata->indexBestNeighbor = 0;
    mydata->bestNeighbor= -1;   

    mydata->last_motion_update = 0;
    mydata->current_light = 0;
    mydata->waiting_time = 0;
    mydata->start_time_behavior = 0;

    mydata->behavior = AGGREGATION;
    srand(rand_hard());
}



#ifdef SIMULATOR

#include <jansson.h>

/**
 * \brief Creates a light circle where the intensity varies between 0 at the edge to 1024 at the center. The center is at 0.
 * \param x the x coordinate of the robot
 * \param y the y coordinate of the robot
 * \return the light measure corresponding to the robot position.
 */
int16_t callback_lighting(double x, double y){
    return MAX(1023  - 7/4*(MIN(abs(x),512) + MIN(abs(y), 512)), 0);
}

int16_t callback_obstacles(double x, double y, double *m1, double *m2){
    if(x*x + y*y >= 1024*1024){
        *m1 = (x<0)? 1:-1;
        *m2 =  (y<0)? 1:-1;
        return 1;
    }
    else{
        return 0;   
    }
}


/**
 * \brief Returns the state of the robot at the current instant.
 * \return the json_t struct corresponding to the state of the robot
 */
json_t *json_state(){
    //create the state object we return
    json_t* state = json_object();

    // store the gradient value
    char *content;
    switch(mydata->state){
        case SEARCHING:
            content = "SEARCHING";
            break;
        case CONVERGING:
            content = "CONVERGING";
            break;
        case SLEEPING:
            content = "SLEEPING";
            break;
        case REPELLING: 
            content = "REPELLING";
            break;
    }
    json_t* g = json_string(content);
    json_object_set (state, "states", g);

    return state;
}

static char botinfo_buffer[10000];

/**
 * \brief Returns the string to display for the simulator
 * \return the char* corresponding to the string to display
 */
char* botinfo(void){

    char *p = botinfo_buffer;
    p += sprintf (p, "ID: %d \n", mydata->current_light);

    return botinfo_buffer;
}
#endif



/**
 * \brief Main function of the behaviors. It calls the function depending of the parameters in the "params.h" file.
 * \return 0 if the behavior has ended normally
 */
int main(void){
    
    kilo_init();
    // initialize ring buffer
    RB_init();
    kilo_message_rx = rxbuffer_push; 
    kilo_message_tx = message_tx;   // register our transmission function
    
    SET_CALLBACK(botinfo, botinfo);
    SET_CALLBACK(reset, setup);
    SET_CALLBACK(lighting, callback_lighting);
    SET_CALLBACK(obstacles, callback_obstacles);
    SET_CALLBACK(json_state, json_state);

    switch(BEHAVIOR){
        case AGGREGATION:
            kilo_start(setup, aggregation);
            break;
        case BEECLUST:
            kilo_start(setup,beeclust);
            break;
        case COVERAGE:
            kilo_start(setup,coverage);
            break;
        case COVERAGE_AGGREGATION:
            kilo_start(setup, loopAggregationCoverage);
            break;
    } 

    return 0;
}
