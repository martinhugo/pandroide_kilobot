/**
  * \file edge.c
  * \brief contains all the edge following functions
  * \author { Maud BEGUEY, Hugo MARTIN }
  * \version 1.0
  * \date July 12 2017
  */


#include <kilombo.h>
#include <limits.h>

#include "edge.h"
#include "params.h"
#include "movement.h"
#include "main.h"
#include "communication.h"

#ifndef SIMULATOR
    #include <avr/io.h>      // for microcontroller register defs 
#endif

extern USERDATA * mydata;

/**
* \brief Return the nearest Neighbor
* \param ignoreNeighborInMovement 1 for ignored the neighbors in movement, 0 otherwise
*/
Neighbor_t getNearestNeighbor(uint8_t ignoreNeighborInMovement){
    int8_t i;
    uint8_t dist = 255;
    Neighbor_t nearestNeighbor;

    for (i = 0; i<mydata->N_Neighbors;i++){
        if(mydata->neighbors[i].dist < dist){

            //if the robot has not to ignore its neighbor in movement or if its neighbors are not in movement
            if(!ignoreNeighborInMovement || !isInMovement(mydata->neighbors[i])){
                dist = mydata->neighbors[i].dist;
                nearestNeighbor = mydata->neighbors[i];
            }
        }
    }

    return nearestNeighbor;
}


/** 
 * \brief returns true if the robot is too close behind a neighbor in movement
 * \return 1 if it is too close behind a neighbor, 0 otherwise
 */
uint8_t tooCloseBehindANeighbor(){
    uint8_t i;
    for(i = 0;i<mydata->N_Neighbors;i++){

        //if the neighbor is at a distance too close from the robot and is in movement
        if(mydata->neighbors[i].dist < MIN_DIST_FOLLOW && (mydata->neighbors[i].state == MOVE_WHILE_INSIDE || mydata->neighbors[i].state == MOVE_WHILE_OUTSIDE)){
            
            //if the nearest neighbor of the neighbor is in the robot history
            if(isInHistory(mydata->neighbors[i].nearestNeighborID)){

                return 0;
            }

            else{
                if(mydata->position.x != 0 || mydata->position.y != 0){
                    return mydata->numberEdgeFollowed <= mydata->neighbors[i].numberEdgeFollowed;
                }

                else if(mydata->state == MOVE_WHILE_OUTSIDE){
                    return (getFreeSlotsNumber() <= MAXHIST/2);
                }

                else{
                    return 0;
                }

            }//end else
        }
    }//end for
    return 0;
}

/**
 * \brief inits the history structure
 */
void initHistory(){
    uint8_t i;
    for(i = 0;i<MAXHIST;i++){
        mydata->history[i] = UINT16_MAX;
    }
}

/**
 * \brief returns true if the given id is in the history 
 */
uint8_t isInHistory(uint16_t ID){
    uint8_t i;
    for(i = 0; i < MAXHIST;i++){
        if(mydata->history[i] == ID){
            return 1;
        }
    }
    return 0;

}

/**
 * \brief returns the number of free slots in the History
 */
uint8_t getFreeSlotsNumber(){
    uint8_t i, freeSlots = 0;
    for(i = 0; i < MAXHIST;i++){
        if(mydata->history[i] == UINT16_MAX){
            freeSlots += 1;
        }
    }
    return freeSlots;
}

/**
 * \brief updates the history  with the new edge followed-robots
 */
void updateHistory(){
    if(!isInHistory(getNearestNeighbor(1).ID)){
        mydata->indexHistory += 1;
        mydata->history[mydata->indexHistory%MAXHIST] = getNearestNeighbor(1).ID;
        if(mydata->position.x != 0  || mydata->position.y !=0){
            mydata->numberEdgeFollowed += 1;
        }
    }
}


/**
  * \brief moves the robot depending on the nearest neigbhor's distance
  */
void follow(){
    if(mydata->N_Neighbors > 0){   
        Neighbor_t neighbor = getNearestNeighbor(1);
        uint8_t dist = neighbor.dist; // getAvgDist(neighbor.distSamples);
        updateHistory();

        // if not behind and too close from an other robot in movement
        if(!tooCloseBehindANeighbor()){ 
            //if the robot is too close from the robot edge followed
            if(dist < DESIRED_DIST){
                if(mydata->previousDist < dist){
                    mydata->direction = STRAIGHT;
                }
                else{
                    mydata->direction = LEFT;
                }
            }

            //if the robot is too distant from the robot edge followed
            else{        
                if(mydata->previousDist > dist){
                    mydata->direction = STRAIGHT;
                }
                else{
                    mydata->direction = RIGHT;
                }

            }   
        }

        // if behind and too close from an other robot in movement
        else{
            mydata->direction = STOP;
        }

        setMotion();
        //printf("%d %d %d %d\n", kilo_uid, mydata->previousDist, dist, getAvgDist(neighbor.distSamples)) ;
        mydata->previousDist = dist;
    }
}



/**
 *  \brief Return 1 if the robot is allowed to move, 0 otherwise.
 *  \return the truth value which determines if the robot is allowed to move
 *
 *   If the robot has the biggest gradient in its neighborhood it's allowed to move.
 *   If other robots have the same gradient, it moves if it has the biggest ID.
 *   If an other robot in its neighborhood is moving, it stays still.
 */
uint8_t isAllowedToMove(){
    uint8_t i;
    uint16_t maxGrad=0, maxID=0, otherRobotMoving = 0;

    for(i=0; i<mydata->N_Neighbors && !otherRobotMoving; i++){
        if(mydata->neighbors[i].state != JOINED_SHAPE){

            // maxGrad is updated if it's greater
            if(!isInMovement(mydata->neighbors[i])){

                if(mydata->neighbors[i].gradient>maxGrad){
                    maxGrad = mydata->neighbors[i].gradient;
                }

                // maxID is updated if the robots has the same gradient and a greater ID than the saved one.
                if((mydata->gradient == mydata->neighbors[i].gradient) && (maxID < mydata->neighbors[i].ID)){
                    maxID = mydata->neighbors[i].ID;
                }

            }
            
            // otherRobotMoving is updated if a robot is moving in the neighborhood
            else{
                otherRobotMoving = 1;
            }
        }//end if mydata->neighbors[i].state != JOINED_SHAPE
    }//end for

    return (!otherRobotMoving) && ((mydata->gradient > maxGrad) || (mydata->gradient == maxGrad && mydata->ID > maxID));
}


