/**
 * \file movement.c
 * \brief  Contains all the movement functins
 * \author { Maud BEGUEY, Hugo MARTIN }
 * \version 1.0
 * \date July 12 2017
 */

#include <kilombo.h>

#include "main.h"

#ifndef SIMULATOR
    #include <avr/io.h>      // for microcontroller register defs 
#endif


extern USERDATA * mydata;

/**
 * \brief indicates if a neighbor is in movement
 * \param neighbor
 * \return 1 if the neighbor is in movement, 0 otherwise
**/
uint8_t isInMovement(Neighbor_t neighbor){
    return (neighbor.state == MOVE_WHILE_OUTSIDE || neighbor.state == MOVE_WHILE_INSIDE);
}


/** 
 * \brief A helper function for setting motor state.
 * \param ccw the left motor value
 * \param cw the right motor value
 * 
 * Automatic spin-up of left/right motor, when necessary.
 * The standard way with spinup_motors() causes a small but noticeable jump when a motor which is already running is spun up again.
 * This method has been taken from Kilombo's examples.
 */
void smoothSetMotors(uint8_t ccw, uint8_t cw){
    // OCR2A = ccw;  OCR2B = cw;  
    #ifdef KILOBOT 
        uint8_t l = 0, r = 0;
        if (ccw && !OCR2A) // we want left motor on, and it's off
            l = 0xff;
        if (cw && !OCR2B)  // we want right motor on, and it's off
            r = 0xff;
        if (l || r)        // at least one motor needs spin-up
         {
            set_motors(l, r);
            delay(15);
          }
    #endif

    // spin-up is done, now we set the real value
    set_motors(ccw, cw);
}

/** 
 * \brief Sets the robot in motion according to the mydata->direction value.
 * 
 * This value has been previously set by one of the method during the switch case on mydata->state
 */
void setMotion(){
    switch (mydata->direction){
        case LEFT:
            smoothSetMotors(kilo_turn_left, 0); 
            break;
        case RIGHT:
            smoothSetMotors(0, kilo_turn_right);
            break;
        case STRAIGHT:
            smoothSetMotors(kilo_straight_left, kilo_straight_right);
            break;
        case STOP:
        default:
            smoothSetMotors(0, 0);
            break;
    }
}
