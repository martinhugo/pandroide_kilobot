#ifndef LOCALID_H
    #define LOCALID_H

    void setLocalID();
    uint8_t isIDUnique();

#endif