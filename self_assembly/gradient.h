#ifndef GRADIENT_H
    #define GRADIENT_H
    void updateGradient();
    void updateGradientWithHysteresis();
#endif 