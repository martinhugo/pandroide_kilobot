#ifndef LOCALIZATION_H
    #define LOCALIZATION_H

    #include "main.h"

    void trilateration();
    void initLocalizedStationaryNeighbors();
    uint8_t isLocalizationCalculable();
    void sortNeighborsByDist();
    uint8_t isLocalized(Neighbor_t robot);
    uint8_t isStationnary(Neighbor_t  robot);
    uint8_t aligned(Neighbor_t  robotA, Neighbor_t  robotB, Neighbor_t robotC);
    double getDistance(Neighbor_t robot, Neighbor_t other);
    void setPosition();
    uint8_t isPositionCorrect(uint8_t delta);
    void updateDirection();
    pos_t nextLocalization(uint8_t coeff);


#endif