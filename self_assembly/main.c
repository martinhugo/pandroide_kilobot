/**
  * \file main.c
  * \brief Main file of the self-assembly algorithm
  * \author { Maud BEGUEY, Hugo MARTIN }
  * \version 1.0
  * \date July 12 2017
  */

#include <kilombo.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>

#include "main.h"
#include "params.h"
#include "edge.h"
#include "communication.h"
#include "gradient.h"
#include "localization.h"
#include "shape.h"
#include "movement.h"
#include "localID.h"

#ifdef SIMULATOR
#include <stdio.h>    // for printf
#else
#define DEBUG         // for printf to serial port
#include "debug.h"
#endif

#define MIN(a,b) (a < b) ? a : b
#define MAX(a,b) (a < b) ? b : a



REGISTER_USERDATA(USERDATA) 

// rainbow colors
const uint8_t colors[] = {
    RGB(0,0,0),  //0 - off
    RGB(2,0,0),  //1 - red
    RGB(2,1,0),  //2 - orange
    RGB(2,2,0),  //3 - yellow
    RGB(1,2,0),  //4 - yellowish green
    RGB(0,2,0),  //5 - green
    RGB(0,1,1),  //6 - cyan
    RGB(0,0,1),  //7 - blue
    RGB(1,0,1),  //8 - purple
    RGB(3,3,3)   //9  - bright white
 };

/** 
 * \brief Initial setup for the current robot. This method is called before the main loop is started.
 */
void setup(){
    mydata->direction = STOP;
    mydata->last_motion_update = 0;
    mydata->N_Neighbors = 0;
    mydata->indexHistory = 0;
    mydata->numberEdgeFollowed = 0;
    mydata->previousDist = 255;
    
    if(kilo_uid == 0){
        mydata->status = GRADIENT_SEED;
        mydata->state = START;
        mydata->ID = 0;
        mydata->gradient = 0;
        mydata->position.x = -24;
        mydata->position.y = 0;
    }
    else if(kilo_uid == 1 || kilo_uid == 2 || kilo_uid == 3){
        mydata->state = START;
        mydata->status = SEED;
        mydata->gradient = USHRT_MAX-1;  

        if(kilo_uid == 1){
            mydata->ID = 1;
            mydata->position.x = 0;
            mydata->position.y = 24;
        }
        else if(kilo_uid == 2){
            mydata->ID = 2;
            mydata->position.x = 24;
            mydata->position.y = 0;
        }
        else if(kilo_uid == 3){
            mydata->ID = 3;
            mydata->position.x = 0;
            mydata->position.y = -24;
        }

    }
    else{
        //srand(rand_hard() + kilo_uid);
        mydata-> status = SIMPLE;
        mydata->gradient = USHRT_MAX-1;
        mydata->position.x = 0;
        mydata->position.y = 0;
        mydata->lastPosition.x = mydata->position.x;
        mydata->lastPosition.y = mydata->position.y;
        mydata->state = START;
        mydata->ID = kilo_uid;
    }
    initHistory();
    setupMessage();
}

/**
 * \brief set the color of a robot depending of its gradient
**/
void displayGradient(){
    set_color(colors[mydata->gradient%10]);
}

/**
 *\brief the main function of the self_assembly algorithm 
 *
 * The main loop of the algorithm comes from the supplementary materials presented in the 
 * article "M. Rubenstein, A. Cornejo and R. Nagpal. "Programmable self-assembly in a thousand-robot swarm". Science, vol. 345, no. 6198, pp. 795–799, 2014."
**/
void selfAssembly(){    
    switch (mydata->state){
        case START:
        
            // if(mydata->status != GRADIENT_SEED && mydata->status != SEED){
            //     setLocalID();
            // }

            updateGradient();

            //robots waits is this state to let their gradient initialize
            if(kilo_ticks >= KILOTICKS_TO_WAIT){

                if(mydata->status != SIMPLE){
                    mydata->state = JOINED_SHAPE;
                }

                else{
                    mydata->state = WAIT_TO_MOVE;
                }

            }

        break;

        case WAIT_TO_MOVE:

            //setLocalID();
            updateGradient();

            initLocalizedStationaryNeighbors();
            trilateration();

            if(isAllowedToMove()){
                mydata->state = MOVE_WHILE_OUTSIDE;
            }

        break;

        case MOVE_WHILE_OUTSIDE:

            //setLocalID();
            updateGradient();

            initLocalizedStationaryNeighbors();
            trilateration();
            
            follow();
            updateDirection();

            //the robot is localized correctly and is in the shape
            if(isPositionCorrect(VERIF_LOCALIZATION_OUTSIDE)){
                if(isInShape()){
                    mydata->state = MOVE_WHILE_INSIDE;
                }
            }

        break;

        case MOVE_WHILE_INSIDE:

            //setLocalID();
            updateGradient();
            initLocalizedStationaryNeighbors();

            trilateration();
            updateDirection();

            follow();

            //the robot is localized correctly and is about to get out of the shape 
            //or is out of the shape or its nearest neighbor has the same gradient
            if(
                (!isInShape() && isPositionCorrect(VERIF_LOCALIZATION_INSIDE)) ||
                (!nextPositionIsInShape(VECT_NEXT_POSITION) && isPositionCorrect(VERIF_LOCALIZATION_INSIDE)) ||
                (getNearestNeighbor(1).gradient >= mydata->gradient)
                ){

                mydata->state = JOINED_SHAPE;
                mydata->direction = STOP;
                setMotion();
            }
            
        break;

        case JOINED_SHAPE:
            initLocalizedStationaryNeighbors();

            //if a robot has to adjust its positions.
            if(!isPositionCorrect(VERIF_LOCALIZATION_SHAPE)){
                if( (mydata->status != SEED && mydata->status != GRADIENT_SEED) || kilo_ticks < 400)
                    trilateration();
            }

        break;

    }
}

/**
 * \brief the main loop of the algorithm
**/
void loop(){

    if(mydata->N_Neighbors == 0){
        setup();
    }

    mydata->last_motion_update = kilo_ticks;
    while (!RB_empty()) {
        processMessage();
        RB_popfront();
    }

    purgeNeighbors();
    setupMessage();

    switch(BEHAVIOR){

        case GRADIENT:
            updateGradient();
            displayGradient();
            break;

        case EDGE:
            if(isAllowedToMove())
                follow();
            break;

        case LOCALIZATION:
            trilateration();
            break;

        case SELF_ASSEMBLY:
        default:
            selfAssembly();
            displayGradient();
        break;
    } 
}



#ifdef SIMULATOR

#include <jansson.h>

/**
 * \brief Creates a light circle where the intensity varies between 0 at the edge to 1024 at the center. The center is at 0.
 * \param x the x coordinate of the robot
 * \param y the y coordinate of the robot
 * \return the light measure corresponding to the robot position.
 */
int16_t callback_lighting(double x, double y){
    return MAX(1023  - 7/4*(MIN(abs(x),512) + MIN(abs(y), 512)), 0);
}

/**
 * \brief creates walls 
 * \param x the x coordinate of the robot
 * \param x the y coordinate of the robot
 * \m1 the x direction of the robot 
 * \m2 the y direction of the robot
 * \return 1 if the robot is in a wall, 0 otherwise
 **/
int16_t callback_obstacles(double x, double y, double *m1, double *m2){
    if(x*x + y*y >= 1024*1024){
        *m1 = (x<0)? 1:-1;
        *m2 =  (y<0)? 1:-1;
        return 1;
    }
    else{
        return 0;   
    }
}


static char botinfo_buffer[10000];

/**
 * \brief Returns the string to display for the simulator
 * \return the char* corresponding to the string to display
 */
char* botinfo(void){

    char *p = botinfo_buffer;
    p += sprintf (p, "ID: %d, Gradt:%d; State: %d, Pos(%.2f, %.2f), N_Neighbors: %d, nbEdgeFollowed: %d, Direction: %d %d\n", mydata->ID, mydata->gradient, mydata->state, mydata->position.x, mydata->position.y, mydata->N_Neighbors, mydata->numberEdgeFollowed, mydata->vectDirection.x, mydata->vectDirection.y);

    return botinfo_buffer;
}
#endif



/**
 * \brief Main function of the behaviors. It calls the function depending of the parameters in the "params.h" file.
 * \return 0 if the behavior has ended normally
 */
int main(void){
    
    kilo_init();
    // initialize ring buffer
    RB_init();

    #ifdef DEBUG
    // setup debugging, i.e. printf to serial port, in real Kilobot
    debug_init();
    #endif

    kilo_message_rx = rxbuffer_push; 
    kilo_message_tx = message_tx;   // register our transmission function
    
    SET_CALLBACK(botinfo, botinfo);
    SET_CALLBACK(reset, setup);
    //SET_CALLBACK(lighting, callback_lighting);
    //SET_CALLBACK(obstacles, callback_obstacles);
    //SET_CALLBACK(json_state, json_state);
    kilo_start(setup, loop);
    
    return 0;
}
