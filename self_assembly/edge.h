#ifndef EDGE_H
    #define EDGE_H
    
    #include "main.h"

    uint8_t getDistByID(uint16_t bot);
    uint8_t  getNearestNeighborDist(void);
    uint16_t getNearestNeighborGradient(void);
    Neighbor_t getNearestNeighbor(uint8_t ignoreNeighborInMovement);
    uint8_t tooCloseBehindANeighbor();
    uint8_t isBehind(Neighbor_t neighbor);
    uint8_t isInHistory(uint16_t ID);
    void follow(void);
    uint8_t isAllowedToMove(void);

    void updateHistory();
    void initHistory();
    uint8_t getFreeSlotsNumber();

#endif
