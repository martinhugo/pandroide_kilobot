/**
  * \file shape.c
  * \brief contains all the shape functions
  * \author { Maud BEGUEY, Hugo MARTIN }
  * \version 1.0
  * \date  July 12 2017
  *
  * Shape as given as C-array. An parser bmp2c needs to be developed/installed to use bitmap images.
  */


#include <kilombo.h>
#include <stdio.h>
#include <math.h>

#include "shape.h"
#include "main.h"
#include "params.h"
#include "localization.h"

extern USERDATA * mydata;


// const uint8_t SHAPE[NB_ROW][NB_COL] = {
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
// };

// const uint8_t SHAPE[NB_ROW][NB_COL] = {
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
// };

// const uint8_t SHAPE[NB_ROW][NB_COL] = {
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//         {1, 1, 0, 0, 0, 0, 0, 0, 1, 1},
//         {1, 1, 0, 0, 0, 0, 0, 0, 1, 1},
//         {1, 1, 1, 1, 1, 1, 0, 0, 1, 1},
//         {1, 1, 1, 1, 1, 1, 0, 0, 1, 1},
//         {0, 0, 0, 0, 0, 0, 0, 0, 1, 1},
//         {0, 0, 0, 0, 0, 0, 0, 0, 1, 1},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
// };

// const uint8_t SHAPE[NB_ROW][NB_COL] = {
//         {1, 1, 0, 0, 0, 0, 0, 0, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
//         {0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
//         {0, 0, 0, 1, 1, 1, 1, 0, 0, 0},
//         {0, 0, 0, 1, 1, 1, 1, 0, 0, 0},
//         {0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
//         {0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 0, 0, 0, 0, 0, 0, 1, 1}
// };


// const uint8_t SHAPE[NB_ROW][NB_COL] = {
//         {0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
//         {0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
//         {0, 0, 0, 1, 1, 1, 1, 0, 0, 0},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//         {0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
//         {0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
//         {0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
//         {0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
//         {1, 1, 1, 1, 0, 0, 1, 1, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1}
// };

// const uint8_t SHAPE[NB_ROW][NB_COL] = {
//         {0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
//         {0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
//         {0, 0, 0, 1, 1, 1, 1, 0, 0, 0},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//         {0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
//         {0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
//         {0, 0, 1, 1, 1, 1, 1, 1, 0, 0},
//         {0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
//         {1, 1, 1, 1, 0, 0, 1, 1, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 1}
// };

const uint8_t SHAPE[NB_ROW][NB_COL] = {
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
        {1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
        {1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
        {1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
        {1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
        {1, 1, 1, 1, 1, 0, 0, 0, 0, 0}
};

// const uint8_t SHAPE[NB_ROW][NB_COL] = {
//         {1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1},
//         {1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1},
//         {1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0},
//         {0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0},
//         {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
//         {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
//         {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0},
//         {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
//         {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
//         {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
//         {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0},
//         {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
//         {0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0},
//         {0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0},
//         {1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1},
//         {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1}
// };

// const uint8_t SHAPE[NB_ROW][NB_COL] = {
//         {1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
//         {1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
//         {1, 1, 1, 0, 0, 0, 0, 0, 0, 0},
//         {1, 1, 1, 1, 0, 0, 0, 0, 0, 0},
//         {1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
//         {1, 1, 1, 1, 1, 1, 0, 0, 0, 0},
//         {1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
//         {1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
//         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
// };

/** 
 * \brief Indicates if the robot is in the shape depending of its known positions
 * \return 1 if the robot is in the shape, 0 otherwise
**/
uint8_t isInShape(){

    if(		
    (mydata->position.x > 0) && (mydata->position.x < (NB_COL - 1) * SIZE_CELL) &&
    (mydata->position.y > 0) && (mydata->position.y < (NB_ROW - 1) * SIZE_CELL)){
        uint8_t ind_x = (int)(mydata->position.x / (double)SIZE_CELL) , ind_y = (int)(mydata->position.y / (double)SIZE_CELL);
        
        return SHAPE[NB_ROW - ind_y - 1][ind_x];
    }
    else{
    	return 0;
    }

}	

/**
 * \brief Indicates if a robot is in the shape for its next position
 * \param coeff the coefficient applied to the vector for the calculation of the next position
 * \return 1 if the robot will be in the shape, 0 otherwise
**/
uint8_t nextPositionIsInShape(double coeff){
    pos_t nextPosition = nextLocalization(coeff);

    if(     
    (nextPosition.x > 0) && (nextPosition.x < (NB_COL - 1) * SIZE_CELL) &&
    (nextPosition.y > 0) && (nextPosition.y < (NB_ROW - 1) * SIZE_CELL)){
        uint8_t ind_x = (int)(nextPosition.x / (double)SIZE_CELL) , ind_y = (int)(nextPosition.y / (double)SIZE_CELL);
        return SHAPE[NB_ROW - ind_y - 1][ind_x];
    }
    else{
        return 0;
    }
}