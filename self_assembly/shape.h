#ifndef SHAPE_H
    #define SHAPE_H 

    uint8_t isInShape(void);
    uint8_t nextPositionIsInShape(double coeff);
    
#endif