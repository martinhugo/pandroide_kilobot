/**
 * \file communication.c
 * \brief  Contains all the communication function
 * \author { Maud BEGUEY, Hugo MARTIN }
 * \version 1.0
 * \date July 12 2017
 */

#include <kilombo.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "params.h"
#include "main.h"
#include "communication.h"
#include "gradient.h"
#include "edge.h"

extern USERDATA * mydata;

/**
 * \brief message rx callback function. Pushes message to ring buffer.
 * \param msg the received message
 * \param dist the measured distance
 * 
 * This method has been taken from Kilombo's examples.
 */
void rxbuffer_push(message_t *msg, distance_measurement_t *dist){
    received_message_t *rmsg = &RB_back();
    rmsg->msg = *msg;
    rmsg->dist = *dist;
    RB_pushback();
}

/**
 * \brief Returns the message to be transmitted
 * \return the message to be transmitted
 * 
 * This method has been taken from Kilombo's examples.
 */
message_t *message_tx() {
    if (mydata->message_lock)
        return 0;
    return &mydata->transmit_msg;
} 

/** 
 * \brief Processes a received message at the front of the ring buffer.
 * 
 * Goes through the list of neighbors. If the message is from a bot already in the list, update the information.
 * 0therwise adds a new entry in the list
 */
void processMessage () {
    uint8_t i;
    uint8_t state;
    uint16_t ID;
    int16_t pos_x = 0, pos_y = 0;
    uint16_t receivedGrad = UINT16_MAX;
    uint8_t d = getSampledDistEstimation(RB_front());
    uint16_t nearestNeighborID = UINT16_MAX;
    uint16_t numberEdgeFollowed = 0;

    uint8_t *data = RB_front().msg.data;
    ID = data[0] | (data[1] << 8);
    state = data[2];

    //if the sender is not in movement
    if(state != MOVE_WHILE_INSIDE && state != MOVE_WHILE_OUTSIDE){
        receivedGrad = data[3] | (data[4] << 8);       // gradient reception
        pos_x = data[5] | (data[6] << 8);              // pos_x reception
        pos_y = data[7] | (data[8] << 8);              // pos_y reception
    }

    //if the sender is in movement
    else{
        nearestNeighborID = data[3] | (data[4] << 8);  // id of the nearest neighbor reception
        numberEdgeFollowed = data[5] | (data[6] << 8);
    }
    
    // search the neighbor list by ID
    uint8_t found = 0;
    i = 0;
    while(i < mydata->N_Neighbors && found==0){
        if (mydata->neighbors[i].ID == ID) {
            found = 1;
        }
        else{
            i++;
        }
    }

    if (found == 0){                        // this neighbor is not in list
        mydata->neighbors[i].indexSample = 0;
        mydata->neighbors[i].numberEdgeFollowed = 0;
        if (mydata->N_Neighbors < MAXN-1)   // if we have too many neighbors, we overwrite the last entry
            mydata->N_Neighbors++;          // sloppy but better than overflow

    }


    // i now points to where this message should be stored
    
    mydata->neighbors[i].ID = ID;
    mydata->neighbors[i].timestamp = kilo_ticks;

    addSample(i, d);
    mydata->neighbors[i].dist = d; //getAvgDist(i);
    
    mydata->neighbors[i].state = state;

    //if the sender is not in movement
    if(state != MOVE_WHILE_OUTSIDE && state != MOVE_WHILE_INSIDE){
        mydata->neighbors[i].position.x = pos_x;
        mydata->neighbors[i].position.y = pos_y;
        mydata->neighbors[i].gradient = receivedGrad;
    }

    //if the sender is in movement
    else{
        mydata->neighbors[i].nearestNeighborID = nearestNeighborID;
        mydata->neighbors[i].numberEdgeFollowed = numberEdgeFollowed;
    }
}



/**
 * \brief Go through the list of neighbors, remove entries older than a threshold, currently 2 seconds.
 */
void purgeNeighbors(void){
    int8_t i;
    for (i = mydata->N_Neighbors-1; i >= 0; i--){
        
        if (kilo_ticks - mydata->neighbors[i].timestamp  > 2*SECOND){  //this one is too old.
            mydata->neighbors[i] = mydata->neighbors[mydata->N_Neighbors-1]; 
            mydata->N_Neighbors--;
          }
    }
}



/**
 * \brief Sets up the message to be transmitted
 * 
 * If the robot is in movement, it sends its ID, state, nearest neighbor id and the number of nieghbors edge followed
 * If the robot is not in movement, its sends its ID, state, gradient, position x and position y
 */
void setupMessage(){

    //si en mouvement, remplacer gradient et coordonnées par direction et historique
    mydata->message_lock = 1;  //don't transmit while we are forming the message
   
    mydata->transmit_msg.type = NORMAL;
    mydata->transmit_msg.data[0] = mydata->ID & 0xff; //0 low  ID
    mydata->transmit_msg.data[1] = mydata->ID >> 8;   //1 high ID

    // state
    mydata->transmit_msg.data[2] = mydata->state;

    //if the robot is not in movement
    if(mydata->state != MOVE_WHILE_OUTSIDE && mydata->state != MOVE_WHILE_INSIDE){

        // gradient
        mydata->transmit_msg.data[3] = mydata->gradient & 0xff; //0 low  gradient
        mydata->transmit_msg.data[4] = mydata->gradient >> 8;   //1 high gradient

        // position x
        mydata->transmit_msg.data[5] = ((int16_t) round(mydata->position.x)) & 0xff;
        mydata->transmit_msg.data[6] = ((int16_t) round(mydata->position.x)) >> 8;

        // position y
        mydata->transmit_msg.data[7] = ((int16_t) round(mydata->position.y)) & 0xff;
        mydata->transmit_msg.data[8] = ((int16_t) round(mydata->position.y)) >> 8;

    }

    //if the robot is in movement
    else{

        // ID nearest neighbor
        uint16_t nearestNeighborID = getNearestNeighbor(1).ID;
        mydata->transmit_msg.data[3] = nearestNeighborID & 0xff; //0 low  gradient
        mydata->transmit_msg.data[4] = nearestNeighborID >> 8;   //1 high gradient

        // nb robot edge followed
        mydata->transmit_msg.data[5] = mydata->numberEdgeFollowed & 0xff;
        mydata->transmit_msg.data[6] = mydata->numberEdgeFollowed & 8;
    
    }
    mydata->transmit_msg.crc = message_crc(&mydata->transmit_msg);
    mydata->message_lock = 0;
}



/**
 * \brief add a sample of the distance to a neighbor
 * \param indexNeighbor the index of the neighbor
 * \param dist the distance to the neighbor
**/
void addSample(uint8_t indexNeighbor, uint8_t dist){
    mydata->neighbors[indexNeighbor].indexSample = (mydata->neighbors[indexNeighbor].indexSample+1)%NB_SAMPLES;
    uint8_t index = mydata->neighbors[indexNeighbor].indexSample;
    mydata->neighbors[indexNeighbor].distSamples[index] = dist;
}

/**
 * \brief calculates the average distance to a neighbor
 * \param the list of the distance samples for a neighbor
 * \return the average distance to a neighbor
**/
uint8_t getAvgDist(uint8_t distSamples[]){
    uint16_t sum = 0;
    uint8_t i, k = 0;   

    for(i = 0; i<NB_SAMPLES; i++){
        if(distSamples[i] != 0){
            sum+= distSamples[i];
            k++;
        }
    }
    
    return (uint8_t)round(sum/(double)k);
}

/** 
 * \brief Sample a large amount of distance sensing on a message to compute efficiently the distance sensing.   
 * \return the estimated distance
 */
uint16_t getSampledDistEstimation(received_message_t msg){
    uint16_t sum = 0;
    uint8_t nbSamples;

    for(nbSamples = 0; nbSamples <10; nbSamples++){
        sum += estimate_distance(&msg.dist);
    }


    return sum/nbSamples;
}