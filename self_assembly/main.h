/**
 * \file main.h
 * \brief  contains the main prototypes and structures of the aggregation-coverage repository
 * \author { Maud BEGUEY, Hugo MARTIN }
 * \version 1.0
 * \date May 9 2017
 */


// TODO : revoir commentaires (ne correspondent vraiment pas pour les énums)

#ifndef MAIN__H
    #define MAIN__H

    /**
     * \def MAXN 
     * \brief The max number of neighbor (for the list of neighbor)
     */
    #define MAXN 25
    
    /**
     * \def MAXHIST
     * \brief the max number of neighbor to save in the history
     */
    #define MAXHIST 10

    /**
     * \def RB_SIZE
     * \brief the size of the ring buffer
     */
    #define RB_SIZE 32  // Ring buffer size. Choose a power of two for faster code

    
    /**
     * \def NB_SAMPLES
     * \brief the size of the ring buffer
     */
    #define NB_SAMPLES 3

    /**
     * \enum behavior
     * \brief the current applied behavior
    */
    enum behavior{
        EDGE,                            /*!< A robot of a given ID edge-follows others  */ 
        LOCALIZATION,            /*!< robots doesn't move and localize themselves */ 
        GRADIENT,                   /*!< robots updates their gradient*/
        SELF_ASSEMBLY         /*!< main self assembly algorithm is applied */ 
    };       

    /**
     * \enum status
     * \brief the status of the robot in the self-assembly algorithm
    */
    enum status{
        GRADIENT_SEED,                   /*!< Only robot with 0 for gradient - one robots only should have this status */ 
        SEED,                                       /*!< Robots used to start the localization with given coordinates */ 
        SIMPLE                                     /*!< normal robots in the self-asembly algorithm*/ 
    };       

     /**
     * \enum state
     * \brief the state of  robot in the behavior.
     */
    enum state {
        START,                                      /*!< The robot is initialized */
        WAIT_TO_MOVE,                      /*!< The robot waits to move */ 
        MOVE_WHILE_OUTSIDE,        /*!< The robot  moves while it is outside the shape */
        MOVE_WHILE_INSIDE,            /*!< The robot moves while it is inside the shape*/
        JOINED_SHAPE                         /*!< The robot has joined the shape and stay put until the end of the experiments*/
    };       

     /**
     * \enum direction
     * \brief the robot's direction
     */
    enum direction {
        STOP,                /*!< The robot is stopped*/ 
        LEFT,                 /*!< The robot is moving to the left */ 
        RIGHT,               /*!< The robot is moving to the right */ 
        STRAIGHT         /*!< The robot is moving straight */ 
    };            


    void setup(void);
    void loop(void);
    int main(void);
    extern char* (*callback_botinfo) (void);
    char* botinfo(void);
    int16_t callback_lighting(double, double);
    int16_t callback_obstacles(double x, double y, double* , double* );


    /**
     * \struct received_message_t
     * \brief a message labeled by the estimated distance
     *  
     * This structures has been taken from Kilombo's examples.
     */
    typedef struct {
        message_t msg;                                  /*!< the content of the message */ 
        distance_measurement_t dist;         /*!< the measured dist of the message */ 
    } received_message_t;

    /**
     * \struc pos_t
     * \brief pos representation for the current robot
     */
    typedef struct {
        double x;         /*!< x coordinate for the robot*/ 
        double y;        /*!< y coordinate for the robot  */ 
    } pos_t;

    /**
     * \struc neighbor_pos_t
     * \brief pos representation for the neighbors
     *
     * With our message sizes, we can only send these coordinates as int16_t. Then it's wise to store them as int16_t to save memory size.
     */
    typedef struct {
        int16_t x;                  /*!< x coordinate for a neighbor */ 
        int16_t y;                  /*!< y coordinate for a neighbor */ 
    } neighbor_pos_t;

    /**
     * \struc  vect_t
     * \brief pos representation for the current robot
     */
    typedef struct {
        int8_t x;               /*!< x value for the direction vect */ 
        int8_t y;               /*!< y value for the direction vect */ 
    } vect_t;

    /**
     * \struct Neighbor_t
     * \brief neighbor representation. 
     */
    typedef struct {
        uint16_t ID;                                              /*!< ID of the neighbor */ 
        uint8_t dist;                                             /*!< the last known dist of the neighbor */ 
        
        uint8_t distSamples[NB_SAMPLES];   /*!< history of last received distances*/ 
        uint8_t indexSample;                            /*!< index of the last sample  */ 
    
        uint16_t gradient;                                  /*!< neighbor's gradient */ 
        neighbor_pos_t position;                      /*!< neighbor's position */ 
        uint8_t state;                                           /*!< neighbor's state  */ 
        uint16_t nearestNeighborID;                /*!< edge-followed robot */ 
        uint16_t numberEdgeFollowed;           /*!< number of edge followed robots */ 
        uint32_t timestamp;                                /*!<the tick where the last information was received from this neighbor ) */ 
    } Neighbor_t;

    /**
     * \struct USERDATA
     * \brief  represents the robot itself and all its beliefs.
     */
    typedef struct{

        uint16_t ID;                                                          /*!< robot's ID */

        Neighbor_t neighbors[MAXN];                         /*!<  List of all neighbors at the moment */
        uint8_t LSNeighbors[MAXN];                           /*!<  List of all localized stationary neighbors at the moment*/
        uint16_t history[MAXHIST];                              /*!<  History of edge-followed neighbors */
        
        uint8_t N_Neighbors;                                        /*!< number of neighbors */
        uint8_t N_LSNeighbors;                                   /*!<  number of localized stationary neighbors */
        uint8_t indexHistory;                                        /*!<  index fo the history */

        uint8_t state;                                                      /*!<  current state */
        uint8_t status;                                                     /*!< robot's status */
        uint8_t direction;                                                /*!< current direction */
        uint8_t previousDist;                                        /*!< last distance */
        
        vect_t vectDirection;                                                /*!< direction vect of the robot */
        pos_t position;                                                          /*!<  robot's position */
        pos_t lastPosition;                                                   /*!<   robot's previous position */
        uint16_t gradient;                                                   /*!<  robot's gradient */
        uint16_t numberEdgeFollowed;                           /*!< number of edge followed robots */

        message_t transmit_msg;                                /*!< the message to transmit */
        uint32_t last_motion_update;                          /*!< last memorized clock */
        char message_lock;                                          /*!< lock on the message to transmit */ 
        received_message_t RXBuffer[RB_SIZE];      /*!< ring buffer of received message */ 
        uint8_t RXHead, RXTail;                                    /*!< start and end of the ring buffer */ 
    } USERDATA;




#endif