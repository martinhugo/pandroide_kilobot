/**
 * \file localization.c
 * \brief  Contains all the localization function
 * \author { Maud BEGUEY, Hugo MARTIN }
 * \version 1.0
 * \date July 12 2017
 */

#include <kilombo.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "localization.h"
#include "params.h"
#include "main.h"
#include "movement.h"
#include "communication.h"
extern USERDATA * mydata;



/**
 * \brief updates the position of the mydata structures depending on the neighborhood
 *
 * If the robot has three localized stationnary robots, it can estimates its own position.
 */
void trilateration(){
    if(isLocalizationCalculable()){
        sortNeighborsByDist();
        setPosition();
    }

    //if a robot doesn't detect any localized stationary robots, it reinit its positions.
    else if(mydata->N_LSNeighbors == 0){
        mydata->position.x = 0;
        mydata->position.y = 0;
        mydata->numberEdgeFollowed = 0;
    }
}

/**
 * \brief Sets the position x,y of the robot in function of its localized stationnary neighbors' position.
 *  
 * The algorithm used to set the position of the robots comes from the supplementary materials presented in the 
 * article "M. Rubenstein, A. Cornejo and R. Nagpal. "Programmable self-assembly in a thousand-robot swarm". Science, vol. 345, no. 6198, pp. 795–799, 2014."
 */
void setPosition(){
    uint8_t i, k;

    Neighbor_t robot;
    robot.ID = mydata->ID;
    robot.state = mydata->state;

    double c, v_x, v_y, n_x, n_y;

    for(k = 0; k<1; k++){
        for(i = 0; i< mydata->N_LSNeighbors; i++){ // on itère sur la liste pour calculer
            robot.position.x = (int16_t) round(mydata->position.x);
            robot.position.y = (int16_t) round(mydata->position.y);

            Neighbor_t other = mydata->neighbors[mydata->LSNeighbors[i]];
            c = getDistance(robot, other);

            if(c != 0){
                v_x = (mydata->position.x - other.position.x)/c;
                v_y = (mydata->position.y - other.position.y)/c;
            }
            else{
                v_x = 0;
                v_y = 0;
            }
            
            n_x = other.position.x + getAvgDist(other.distSamples) * v_x;
            n_y = other.position.y + getAvgDist(other.distSamples) * v_y;
            
            mydata->position.x -=  ((mydata->position.x - n_x) / 4);
            mydata->position.y -=  ((mydata->position.y - n_y) / 4);

        }

    }

}

/**
 * \brief Returns 1 if the positions are correct (with an authorized delta) for all neighbors localized, 0 otherwise
 */
uint8_t isPositionCorrect(uint8_t delta){
    uint8_t  i;
    double c;

    Neighbor_t robot;
    robot.ID = mydata->ID;
    robot.state = mydata->state;
    robot.position.x = (int) round(mydata->position.x);
    robot.position.y = (int) round(mydata->position.y);

    for(i = 0; i<mydata->N_LSNeighbors; i++){
        Neighbor_t neighbor = mydata->neighbors[mydata->LSNeighbors[i]];
        c = getDistance(robot, neighbor);

        //if the difference between the distance and the localization is too important
        if(fabs(neighbor.dist - c)  > delta){
            return 0;
        }
    }

    return 1;
}

/**
 * \brief Sorts the localized stationnary neighbors in the decreasing order by distance
 */
void sortNeighborsByDist(){
    uint8_t i, j;

    for(i = 0;i<mydata->N_LSNeighbors; i++){
        for (j = i+1;j<mydata->N_LSNeighbors; j++){

            if(mydata->neighbors[mydata->LSNeighbors[i]].dist > mydata->neighbors[mydata->LSNeighbors[j]].dist){
                Neighbor_t neighbor = mydata->neighbors[mydata->LSNeighbors[i]];
                mydata->neighbors[mydata->LSNeighbors[i]] = mydata->neighbors[mydata->LSNeighbors[j]];
                mydata->neighbors[mydata->LSNeighbors[j]] = neighbor;
            }

        }
    }
}

/**
 * \brief return the list of the stationary neighbors
 * \param *nbLSNeighbors the list of localized stationary neighbors
 * \return list of the localized stationary neighbors
 */
void initLocalizedStationaryNeighbors(){

    Neighbor_t currentRobot;
    uint8_t  i;
    mydata->N_LSNeighbors = 0;
 
    for (i = 0; i < mydata->N_Neighbors; i++){
        currentRobot = mydata->neighbors[i];
        if(isLocalized(currentRobot) && isStationnary(currentRobot)){
            mydata->LSNeighbors[mydata->N_LSNeighbors] = i; 
            mydata->N_LSNeighbors+=1;
        }
    }

}


/**
 * \brief Returns true if 3 neighbors are localized, stationnary and not aligned.
 * \param *LSNeighbors the list of localized stationary neighbors
 * \param nbLSNeighbors the number of localized stationaly neighbors
 * \return true if it is possible to calculate the localization of the robot, false otherwise.
 */
uint8_t isLocalizationCalculable(){
    uint8_t i, j, k;
    Neighbor_t robotA, robotB, robotC;

    for(i=0; i<mydata->N_LSNeighbors; i++){
        robotA = mydata->neighbors[mydata->LSNeighbors[i]];

        for(j=i+1; j<mydata->N_LSNeighbors; j++){
            robotB = mydata->neighbors[mydata->LSNeighbors[j]];

            for(k=j+1; k<mydata->N_LSNeighbors; k++){
                robotC = mydata->neighbors[mydata->LSNeighbors[k]];
                if(!aligned(robotA, robotB, robotC)){
                    return 1;
                }
            }
        } 
    }
    return 0;
}

/**
 * \brief returns true if the robot is localized
 * \param robot the robot
 * \return true if the robot is localized, false otherwise
 */
uint8_t isLocalized(Neighbor_t robot){
    return !(robot.position.x == 0 && robot.position.y == 0);    
}

/**
 * \brief returns true if the robot is stationnary
 * \param robot the robot
 * \return true if the robot is stationnary, false otherwise
 */
uint8_t isStationnary(Neighbor_t  robot){
    return robot.state == JOINED_SHAPE;//(robot.state != MOVE_WHILE_INSIDE && robot.state != MOVE_WHILE_OUTSIDE);
    //return (robot.state != MOVE_WHILE_INSIDE && robot.state != MOVE_WHILE_OUTSIDE);
}

/**
 * \brief returns true if the three robots are aligned
 * \param robotA the first robot
 * \param robotB the second robot
 * \param robotC the third robot
 * \return true if the 3 robots are aligned, false otherwise
 */
uint8_t aligned(Neighbor_t  robotA, Neighbor_t  robotB, Neighbor_t robotC){
    return (fabs(getDistance(robotA, robotB) + getDistance(robotB, robotC) - getDistance(robotA, robotC)) < 5 ||
            fabs(getDistance(robotA, robotC) + getDistance(robotC, robotB) - getDistance(robotA, robotB)) < 5 ||
            fabs(getDistance(robotB, robotA) + getDistance(robotA, robotC) - getDistance(robotB, robotC)) < 5);
}

/**
 * \brief returns the distance between the two robots
 * \param robot the first robot
 * \param other the second robot
 * \return the distance between robot and other based on their positions x and y.
 */
double getDistance(Neighbor_t robot, Neighbor_t other){
    return sqrt(pow((robot.position.x - other.position.x),2) + pow((robot.position.y - other.position.y),2));
}


/**
 * \brief updates the vectDirection vect in function of the last two positions of the robot
 * 
 * the last positions x and y are updates only if a robot moves sufficiently.
 */
void updateDirection(){
    double diff_x = mydata->position.x - mydata->lastPosition.x;
    double diff_y = mydata->position.y - mydata->lastPosition.y;

     if ((fabs(diff_x) + fabs(diff_y) > 2)){

        if(fabs(diff_x) <= 0.5)
            mydata->vectDirection.x = 0;
        else
            mydata->vectDirection.x = (int8_t)(2 * (diff_x > 0) - 1);

        if(fabs(diff_y) <= 0.5)
            mydata->vectDirection.y = 0;
        else
            mydata->vectDirection.y = (int8_t)(2 * (diff_y > 0) - 1);

        mydata->lastPosition.x = mydata->position.x;
        mydata->lastPosition.y = mydata->position.y;
    }

    
}

/**
 * \brief returns the next position depending on the given coeff and the calculated vectDirection 
 * \return the next estimated position of the robot
 */
pos_t nextLocalization(uint8_t coeff){
    pos_t next;
    next.x = mydata->position.x + coeff * mydata->vectDirection.x;
    next.y = mydata->position.y + coeff * mydata->vectDirection.y;

    return next;
}

/** 
  * \brief returns 1 if the given neighbor is in the same direction than the robot
  * \return 1 if the given neighor is moving in the same direction, 0 otherwise
  *
  * this method is never used
  */
uint8_t isInMyDirection(Neighbor_t neighbor){
    double diff_x = neighbor.position.x - mydata->position.x;
    double diff_y = neighbor.position.y - mydata->position.y;
    int8_t direction_x, direction_y;

    //a kilobot has a diameter of 33mm
    if(fabs(diff_x < 17)){
        direction_x = 0;
    }
    else{
        direction_x = (int8_t)(2 * (diff_x > 0) - 1);
    }


    if(fabs(diff_y < 17)){
        direction_y = 0;
    }
    else{
        direction_y = (int8_t)(2 * (diff_y > 0) - 1);
    }

    return (direction_y == mydata->vectDirection.x && direction_x == mydata->vectDirection.y);
}