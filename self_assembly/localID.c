#include <kilombo.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <math.h>

#include "main.h"
#include "localID.h"

#ifndef SIMULATOR
    #include <avr/io.h>      // for microcontroller register defs 
#endif

extern USERDATA * mydata;

// /**
//  * \brief Modify the local ID if this ID is incorrect or not unique
// **/
// void setLocalID(){
//     uint8_t IDunique = isIDUnique();
//     if(IDunique == 0){
//         mydata->ID = (uint16_t)rand();
//     }
// }

// /**
//  * \brief Return 1 if the ID is unique, 0 otherwise 
// **/
// uint8_t isIDUnique(){
//     uint8_t i;

//     if(mydata->ID <=3 || mydata->ID == UINT16_MAX){
//         return 0;
//     }

//     for(i = 0;i<mydata->N_Neighbors;i++){
//         if(mydata->neighbors[i].ID == mydata->ID){
//             return 0;
//         }
//     }
//     return 1;
// }

