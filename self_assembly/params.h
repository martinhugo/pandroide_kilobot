/**
  * \file params.h
  * \brief Contains all the parameters applied to the self-assembly algorithm.
  * \author { Maud BEGUEY, Hugo MARTIN }
  * \version 1.0
  * \date July 12 2017
  */


#ifndef PARAMETERS_H
    #define PARAMETERS_H
    
    /**
     * \def SECOND
     * \brief number of ticks in a second
     */
    #define SECOND 32

    /**
     * \def BEHAVIOR
     * \brief the algorithm to run (GRADIENT, EDGE, LOCALIZATION, SELF_ASSEMBLY)
    */
    #define BEHAVIOR SELF_ASSEMBLY

    /** 
     * \def COORD_INIT
     * \brief the initial coordinates of the robots
     */
    #define COORD_X_INIT 0
    #define COORD_Y_INIT 0

    /** 
     * \def DESIRED_DIST 
     * \brief the distance desired between the robot which move and its neighbors which wait
     */
    #define DESIRED_DIST 48

    /**
     * \DEF MAX_DIST_FOLLOW
     * \brief the maximal distance between two robots in movement
    */
    #define MIN_DIST_FOLLOW 100

    /** 
     * \def GRADIENT_DIST
     * \brief the maximum distance of the neighbors to calculate the gradient
     */
    #define GRADIENT_DIST 55


    /** COVERAGE PARAMETERS **/

    /**
     * \def NB_COL
     * \brief the number of column in the shape
     */
    #define NB_COL 10

    /**
     * \def NB_ROW
     * \brief the number of row in the shape
     */
    #define NB_ROW 10

    /**
     * \def SIZE_CELL
     * \brief the number of mm in a cell of the shape
     */
    #define SIZE_CELL 55

    /**
     * \def KILOTICKS_TO_WAIT
     * \brief the number of kiloticks to wait at the beginning of the algorithm
     * It is used to let all robots initialized their gradient
    */
    #define KILOTICKS_TO_WAIT 200
    
    /**
     * \def VERIF_LOCALIZATION_OUTSIDE
     * \brief the number of millimeters authorized at error in the localization in the MOVE_WHILE_OUTSIDE state
     */
    #define VERIF_LOCALIZATION_OUTSIDE 5

    /**
     * \def VERIF_LOCALIZATION_OUTSIDE
     * \brief the number of millimeters authorized at error in the localization in the MOVE_WHILE_INSIDE state
     */
    #define VERIF_LOCALIZATION_INSIDE 5

    /**
     * \def VERIF_LOCALIZATION_OUTSIDE
     * \brief the number of millimeters authorized at error in the localization in the JOINED_SHAPE state
     */
    #define VERIF_LOCALIZATION_SHAPE 2
    
    /**
     * \def VECT_NEXT_POSITION
     * \brief the number of millimeters to add to the position considering the vector direction
     */
    #define VECT_NEXT_POSITION 5


#endif


