#ifndef MOVEMENT_H
    #define MOVEMENT_H

    uint8_t isInMovement(Neighbor_t neighbor);
    void smoothSetMotors(uint8_t ccw, uint8_t cw);
    void setMotion();

#endif