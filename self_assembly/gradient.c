/**
  * \file gradient.c
  * \brief contains all the functions used to calculate and update the gradient
  * \author { Maud BEGUEY, Hugo MARTIN }
  * \version 1.0
  * \date  July 12 2017
  */

#include <kilombo.h>
#include <limits.h>

#include "gradient.h"
#include "params.h"
#include "communication.h"
#include "main.h"
#include "movement.h"

extern USERDATA * mydata;

/**
 * \brief update the gradient depending on the received gradient value
 * 
 * The gradient is updated to the minimum gradient among a robot neighborhood inside a given radius + 1
 * The neighbors in movement are not taken into account for the gradient update.
**/
void updateGradient(){
    if(mydata->status != GRADIENT_SEED){
        uint16_t minGrad = USHRT_MAX-1;
        uint8_t i;

        for(i = 0; i<mydata->N_Neighbors; i++){
           if(
                mydata->neighbors[i].gradient < minGrad && 
                mydata->neighbors[i].dist <= GRADIENT_DIST && 
                !isInMovement(mydata->neighbors[i])){

                    minGrad = mydata->neighbors[i].gradient;
            }
        }

        mydata->gradient = minGrad + 1;
    }
}

/**
 * \brief update the gradient depending on the received gradient value, with a hysteresis
 * 
 * The gradient is updated to the minimum gradient among a robot neighborhood inside a given radius + 1
 * The neighbors in movement are not taken into account for the gradient update.
 *
 * Not used in this iteration.
**/
void updateGradientWithHysteresis(){
    if(mydata->status != GRADIENT_SEED){
        uint8_t i;
        uint16_t minGradMinRadius = UINT16_MAX-1;
        uint16_t minGradMaxRadius = UINT16_MAX-1;

        for(i = 0;i<mydata->N_Neighbors;i++){

            // updates the minGradMinRadius var depending of the not in movement neighbors in a 70 radius 
            // calculates the min grad in the GRADIENT_DIST radius 
            if(
                mydata->neighbors[i].gradient < minGradMinRadius && 
                mydata->neighbors[i].dist <= GRADIENT_DIST && 
                !isInMovement(mydata->neighbors[i])){

                    minGradMinRadius = mydata->neighbors[i].gradient;

            }

            // updates the minGradMaxRadius var depending of the not in movement neighbors from a GRADIENT_DIST + 5 radius 
            // calculates the min grad from the GRADIENT_DIST + 5 radius 
            if(
                mydata->neighbors[i].gradient < minGradMaxRadius && 
                mydata->neighbors[i].dist <= (GRADIENT_DIST + 5) && 
                !isInMovement(mydata->neighbors[i])){

                    minGradMaxRadius = mydata->neighbors[i].gradient;

            }

        }//end for

        //if the gradient measured in a GRADIENT_DIST radius increases
        if(mydata->gradient <= minGradMinRadius + 1){
            mydata->gradient = minGradMinRadius + 1;
        }

        //else if the gradient measured from a GRADIENT_DIST + 5 radius doesn't change the value of the gradient
        else if (mydata->gradient == minGradMaxRadius + 1){
            mydata->gradient = minGradMaxRadius + 1;
        }
        
        else{
            mydata->gradient = minGradMinRadius + 1;
        }

    }//end if status != GRADIENT_SEED
}



